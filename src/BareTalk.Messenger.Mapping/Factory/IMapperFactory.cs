using BareTalk.Messenger.Mapping.Mapper;

namespace BareTalk.Messenger.Mapping.Factory
{
    public interface IMapperFactory
    {
        IObjectMapper Create();
    }
}
