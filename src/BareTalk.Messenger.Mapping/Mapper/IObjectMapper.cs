namespace BareTalk.Messenger.Mapping.Mapper
{
    public interface IObjectMapper
    {
        R Map<T, R>(T value);
    }
}
