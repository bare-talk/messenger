using System;
using Mapster;

namespace BareTalk.Messenger.Mapping.Mapper
{
    public sealed class AdaptedMapper : IObjectMapper
    {
        private readonly TypeAdapterConfig adapterConfiguration;

        public AdaptedMapper(TypeAdapterConfig adapterConfiguration)
        {
            this.adapterConfiguration = adapterConfiguration;
        }

        public R Map<T, R>(T value)
        {
            return value != null
                  ? value.Adapt<R>(adapterConfiguration)
                  : throw new ArgumentNullException(nameof(value), $"Value for {typeof(T)} was null when attempting to map to {typeof(R)}.");
        }
    }
}
