using System;

namespace BareTalk.Messenger.Mapping.Exceptions
{
    [Serializable]
    public sealed class MappingException : Exception
    {
        public MappingException(string message) : base(message)
        {
        }

        public MappingException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public MappingException() : base()
        {
        }
    }
}
