using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;
using BareTalk.Messenger.Mapping.Factory;
using BareTalk.Messenger.Mapping.Mapper;
using BareTalk.Messenger.Pki.Models;
using Mapster;

namespace BareTalk.Messenger.Pki.Mapper
{
    public sealed class PkiMapperFactory : IMapperFactory
    {
        public IObjectMapper Create()
        {
            TypeAdapterConfig mapperConfiguration = CreateMappings();
            return new AdaptedMapper(mapperConfiguration);
        }

        private TypeAdapterConfig CreateMappings()
        {
            var configuration = new TypeAdapterConfig();
            configuration
                .ForType<AvailableKeyModel, AvailableContactModel>()
                .ConstructUsing(src => new AvailableContactModel(
                    new GuidIdentifier(src.Id),
                    new Username(src.OwnerUsername),
                    new DisplayName(src.DisplayName)
                ));

            configuration
               .ForType<KeyPairModel, PublishKeyModel>()
               .ConstructUsing(src => new PublishKeyModel
               {
                   OwnerUsername = "",
                   Base64EncodedKey = src.PublicKey.Value
               });

            return configuration;
        }
    }
}
