using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.Pki;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Mapping.Mapper;
using BareTalk.Messenger.Pki.Models;
using BareTalk.Messenger.Pki.Rest;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Pki.Publisher
{
    public sealed class KeyPublisher : IPublishKeys
    {
        private readonly ILogger<KeyPublisher> logger;
        private readonly IPkiRestService pkiRestService;
        private readonly IObjectMapper mapper;

        public KeyPublisher(
            ILogger<KeyPublisher> logger,
            IPkiRestService pkiRestService,
            IObjectMapper mapper
        )
        {
            this.logger = logger;
            this.pkiRestService = pkiRestService;
            this.mapper = mapper;
        }

        public async Task<bool> PublishRangeAsync(IEnumerable<KeyPairModel> publicKeys, UserModel user)
        {
            foreach (KeyPairModel key in publicKeys)
            {
                PublishKeyModel publishKeyModel = mapper.Map<KeyPairModel, PublishKeyModel>(key);
                publishKeyModel.OwnerUsername = user.Username.Value;
                publishKeyModel.DisplayName = user.DisplayName.Value;
                var succeeded = await pkiRestService.PublishKeyAsync(publishKeyModel);

                if (!succeeded)
                {
                    logger.LogError("Aborting publish due to an error.");
                    return false;
                }
            }

            logger.LogError("Successfully published all public keys");
            return true;
        }
    }
}
