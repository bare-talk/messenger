using System;

namespace BareTalk.Messenger.Pki.Models
{
    public sealed class AvailableKeyModel : IApiModel
    {
        public Guid Id { get; set; } = Guid.Empty;
        public string OwnerUsername { get; set; } = "";
        public string DisplayName { get; set; } = "";
    }
}
