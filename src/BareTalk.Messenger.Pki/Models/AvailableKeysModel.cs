using System.Collections.Generic;
using BareTalk.Messenger.Serialization.Deserializer;

namespace BareTalk.Messenger.Pki.Models
{
    public sealed class AvailableKeysModel : IApiModel, IAmDeSerializable
    {
        public IEnumerable<AvailableKeyModel> MatchingKeys { get; set; } = new List<AvailableKeyModel>();
    }
}
