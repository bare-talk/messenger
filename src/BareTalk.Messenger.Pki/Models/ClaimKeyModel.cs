using BareTalk.Messenger.Serialization.Deserializer;

namespace BareTalk.Messenger.Pki.Models
{
    public sealed class ClaimKeyModel : IApiModel, IAmDeSerializable
    {
        public string Id { get; set; } = "";
        public string Base64EncodedKey { get; set; } = "";
        public string OwnerUsername { get; set; } = "";
        public string DisplayName { get; set; } = "";
    }
}
