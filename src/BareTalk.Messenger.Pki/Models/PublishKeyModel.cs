using BareTalk.Messenger.Serialization.Serializer;

namespace BareTalk.Messenger.Pki.Models
{
    public sealed class PublishKeyModel : IApiModel, IAmSerializable
    {
        public string Base64EncodedKey { get; set; } = "";
        public string OwnerUsername { get; set; } = "";
        public string DisplayName { get; set; } = "";
    }
}
