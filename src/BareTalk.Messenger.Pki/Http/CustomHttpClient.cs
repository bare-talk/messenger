using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Pki.Http
{
    public sealed class CustomHttpClient : IHttpClient
    {
        private readonly HttpClient httpClient;
        private readonly ILogger<CustomHttpClient> logger;

        public CustomHttpClient(HttpClient httpClient, ILogger<CustomHttpClient> logger)
        {
            this.httpClient = httpClient;
            this.logger = logger;
        }

        public async Task<HttpResponseMessage> PostAsync(string path, HttpContent content)
        {
            try
            {
                return await httpClient.PostAsync(path, content);
            }
            catch (Exception exception)
            {
                logger.LogError("Error encountered when POSTING to {@Url}. Error: {@Error}", httpClient.BaseAddress + path, exception.Message);
            }

            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        public async Task<HttpResponseMessage> GetAsync(string path)
        {
            try
            {
                return await httpClient.GetAsync(path);
            }
            catch (Exception exception)
            {
                logger.LogError("Error encountered when GETTING to {@Url}. Error: {@Error}", httpClient.BaseAddress + path, exception.Message);
            }

            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }
    }
}
