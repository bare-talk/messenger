using System.Net.Http;
using System.Threading.Tasks;

namespace BareTalk.Messenger.Pki.Http
{
    public interface IHttpClient
    {
        Task<HttpResponseMessage> GetAsync(string path);

        Task<HttpResponseMessage> PostAsync(string path, HttpContent content);
    }
}
