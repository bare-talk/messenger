using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Domain.Contracts.Pki;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;
using BareTalk.Messenger.Mapping.Mapper;
using BareTalk.Messenger.Pki.Models;
using BareTalk.Messenger.Pki.Rest;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Pki.Claimer
{
    public sealed class PublicKeyClaimer : IClaimPublicKeys
    {
        private readonly IPkiRestService pkiRestService;
        private readonly IObjectMapper mapper;
        private readonly IKeyPairService keyPairService;
        private readonly ILogger<PublicKeyClaimer> logger;

        public PublicKeyClaimer(IPkiRestService pkiRestService, IObjectMapper mapper, IKeyPairService keyPairService, ILogger<PublicKeyClaimer> logger)
        {
            this.pkiRestService = pkiRestService;
            this.mapper = mapper;
            this.keyPairService = keyPairService;
            this.logger = logger;
        }

        public async Task<KeyPairModel?> ClaimKeyAsync(Username username)
        {
            ClaimKeyModel? claimedKey = await pkiRestService.ClaimOneyKeyForAsync(username.Value);
            if (claimedKey == null)
            {
                logger.LogError("Claiming a key failed.");
                return null;
            }

            KeyPairModel? createdKeyPair = await keyPairService.SavePartialPair(new PublicKey(claimedKey.Base64EncodedKey));
            if (createdKeyPair == null)
            {
                logger.LogError("Failed to save the claimed key.");
                return null;
            }

            logger.LogInformation("The key pair was saved successfully.");
            return createdKeyPair;
        }

        public async Task<IEnumerable<AvailableContactModel>> QueryAvailableKeysFor(Username me, Username username)
        {
            AvailableKeysModel availableKeys = await pkiRestService.GetAvailableKeysAsync(me.Value, username.Value);
            return mapper.Map<IEnumerable<AvailableKeyModel>, IEnumerable<AvailableContactModel>>(availableKeys.MatchingKeys);
        }
    }
}
