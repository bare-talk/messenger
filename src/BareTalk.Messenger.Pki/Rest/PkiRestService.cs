using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BareTalk.Messenger.Pki.Http;
using BareTalk.Messenger.Pki.Models;
using BareTalk.Messenger.Serialization.Deserializer;
using BareTalk.Messenger.Serialization.Serializer;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Pki.Rest
{
    public sealed class PkiRestService : IPkiRestService
    {
        private readonly ISerializer serializer;
        private readonly IDeserializer deserializer;
        private readonly ILogger<PkiRestService> logger;
        private readonly IHttpClient httpClient;

        public PkiRestService(
            ISerializer serializer,
            IDeserializer deserializer,
            ILogger<PkiRestService> logger,
            IHttpClient httpClient
        )
        {
            this.serializer = serializer;
            this.deserializer = deserializer;
            this.logger = logger;
            this.httpClient = httpClient;
        }

        public async Task<bool> PublishKeyAsync(PublishKeyModel publishKeyModel)
        {
            var json = serializer.Serialize(publishKeyModel);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            logger.LogTrace("Attempting to publish a public key.");

            HttpResponseMessage response = await httpClient.PostAsync("/keys", content);
            if (response.IsSuccessStatusCode)
            {
                logger.LogTrace("The key was published successfully.");
                return true;
            }

            logger.LogError("The public key was not published successfully.");
            return false;
        }

        public async Task<ClaimKeyModel?> ClaimKeyAsync(Guid id)
        {
            logger.LogTrace("Attempting to claim key {@Key}", id);
            HttpResponseMessage response = await httpClient.GetAsync($"/keys/{id}/claim");
            if (!response.IsSuccessStatusCode)
            {
                logger.LogError("Failed to claim key {@Key}", id);
                return null;
            }

            logger.LogError("The public key {@Key} was claimed successfully.");
            var responseBody = await response.Content.ReadAsStringAsync();
            return deserializer.Deserialize<ClaimKeyModel>(responseBody);
        }

        public async Task<AvailableKeysModel> GetAvailableKeysAsync(string me, string username)
        {
            logger.LogTrace("Attempting to list keys for username query {@Username}", username);
            HttpResponseMessage response = await httpClient.GetAsync($"/keys/exclude/{me}?usernameQuery={username}");
            if (!response.IsSuccessStatusCode)
            {
                logger.LogError("Failed to list keys.");
                return new AvailableKeysModel();
            }

            logger.LogTrace("Successfully listed keys for username query {@Username}", username);
            var responseBody = await response.Content.ReadAsStringAsync();
            return deserializer.Deserialize<AvailableKeysModel>(responseBody) ?? new AvailableKeysModel();
        }

        public async Task<ClaimKeyModel?> ClaimOneyKeyForAsync(string username)
        {
            logger.LogTrace("Attempting to claim one key for {@Username}", username);
            HttpResponseMessage response = await httpClient.GetAsync($"/{username}/keys/claim");
            if (!response.IsSuccessStatusCode)
            {
                logger.LogTrace("Failed to claim the key");
                return null;
            }
            var responseBody = await response.Content.ReadAsStringAsync();
            return deserializer.Deserialize<ClaimKeyModel>(responseBody);
        }
    }
}
