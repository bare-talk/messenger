using System;
using System.Threading.Tasks;
using BareTalk.Messenger.Pki.Models;

namespace BareTalk.Messenger.Pki.Rest
{
    public interface IPkiRestService
    {
        Task<ClaimKeyModel?> ClaimKeyAsync(Guid id);

        Task<ClaimKeyModel?> ClaimOneyKeyForAsync(string username);

        Task<AvailableKeysModel> GetAvailableKeysAsync(string me, string username);

        Task<bool> PublishKeyAsync(PublishKeyModel publishKeyModel);
    }
}
