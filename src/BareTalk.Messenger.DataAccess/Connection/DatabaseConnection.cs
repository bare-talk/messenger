using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.DataAccess;
using Microsoft.Extensions.Logging;
using SQLite;

namespace BareTalk.Messenger.DataAccess.Connection
{
    public sealed class DatabaseConnection : IDatabaseConnection
    {
        private SQLiteAsyncConnection database = null!;
        private readonly ILogger<IDatabaseConnection> logger;

        public DatabaseConnection(ILogger<IDatabaseConnection> logger)
        {
            this.logger = logger;
        }

        public async Task<bool> CreateTableAsync<T>() where T : IEntity, new()
        {
            var tableName = typeof(T).Name;
            logger.LogTrace("Creating table {@Table}", tableName);
            CreateTableResult result = await database.CreateTableAsync<T>();
            logger.LogInformation("Table {@Table} was {@What}", tableName, result.GetType().Name);
            return true;
        }

        public async Task<bool> InsertAsync<T>(T entity) where T : IEntity
        {
            var entityName = typeof(T).Name;
            logger.LogTrace("Inserting a new {@Entity}", entityName);
            var numberOfRows = await database.InsertAsync(entity);
            if (numberOfRows != 1)
            {
                logger.LogWarning("{@Entity} was not inserted.");
                return false;
            }

            logger.LogInformation("{@Entity} was successfully inserted.", entityName);
            return true;
        }

        public async Task<bool> DeleteAsync<T>(T entity) where T : IEntity, new()
        {
            var entityName = typeof(T).Name;
            logger.LogTrace("Deleting entity {@Entity}", entityName);
            var numberOfRows = await database.DeleteAsync(entity);
            if (numberOfRows != 1)
            {
                logger.LogWarning("{@Entity} was not deleted.");
                return false;
            }

            logger.LogInformation("{@Entity} was successfully deleted.", entityName);
            return true;
        }

        public async Task<T?> FindOneAsync<T>(Expression<Func<T, bool>> predicate) where T : class, IEntity, new()
        {
            var entityName = typeof(T).Name;
            var predicateTextualRepresentation = predicate.ToString();
            logger.LogTrace("Finding one element of type {@Entity} with predicate {@Predicate}", entityName, predicateTextualRepresentation);
            T result = await database
                .Table<T>()
                .FirstOrDefaultAsync(predicate);

            if (result == null)
            {
                logger.LogWarning("No element of type {@Entity} matched predicate {@Predicate}.", entityName, predicateTextualRepresentation);
                return null;
            }

            logger.LogDebug("One element of type {@Entity} matched predicate {@Predicate}", entityName, predicateTextualRepresentation);
            return result;
        }

        public async Task<IEnumerable<T>> FindAllAsync<T>(Expression<Func<T, bool>> predicate, Expression<Func<T, bool>>? orderBy = null) where T : IEntity, new()
        {
            var entityName = typeof(T).Name;
            var predicateTextualRepresentation = predicate.ToString();
            logger.LogTrace("Finding all elements of type {@Entity} with predicate {@Predicate}", entityName, predicateTextualRepresentation);

            if (orderBy != null)
            {
                var orderByTextualRepresentation = orderBy.ToString();
                logger.LogDebug("Detected ordering expression {@OrderBy}", orderByTextualRepresentation);
            }

            AsyncTableQuery<T>? results = orderBy != null ?
                 database
                    .Table<T>()
                    .Where(predicate)
                    .OrderBy(orderBy)
                : database
                    .Table<T>()
                    .Where(predicate);

            if (results != null)
            {
                List<T> enumeratedResults = await results.ToListAsync();
                logger.LogDebug("Found {@Number} number of elements of type {@Entity} matching predicate {@Predicate}", enumeratedResults.Count, entityName, predicateTextualRepresentation);
                return enumeratedResults;
            }

            logger.LogWarning("No elements of type {@Entity} match predicate {@Predicate}", entityName, predicateTextualRepresentation);
            return new List<T>();
        }

        public async Task<bool> UpdateAsync<T>(T entity) where T : IEntity, new()
        {
            var entityName = typeof(T).Name;
            logger.LogTrace("Updating entity {@Entity}", entityName);
            var numberOfUpdatedColumns = await database.UpdateAsync(entity);
            if (numberOfUpdatedColumns == 0)
            {
                logger.LogWarning("No columns were updated for entity {@Entity}", entityName);
                return false;
            }

            logger.LogDebug("Successfully updated {@Entity}", entityName);
            return true;
        }

        public async Task<bool> DropTableAsync<T>() where T : IEntity, new()
        {
            var entityName = typeof(T).Name;
            logger.LogTrace("Dropping table for entity {@Entity}", entityName);
            var droppedTables = await database.DropTableAsync<T>();
            if (droppedTables == 0)
            {
                logger.LogWarning("No tables were dropped for entity {@Entity}", entityName);
                return false;
            }

            logger.LogTrace("Successfully dropped table for entity {@Entity}", entityName);
            return true;
        }

        public async Task<bool> DropDatabaseAsync()
        {
            logger.LogTrace("Dropping database...");
            foreach (TableMapping? table in database.TableMappings)
            {
                logger.LogTrace("Dropping table {@Table}", table.TableName);
                var droppedTables = await database.DropTableAsync(table);
                if (droppedTables == 0)
                {
                    logger.LogWarning("The table {@Table} was not dropped", table.TableName);
                    return false;
                }
                logger.LogTrace("Successfully dropped table {@Table}", table.TableName);
            }

            logger.LogTrace("Successfully dropped database");
            return true;
        }

        public async Task<bool> CloseAsync()
        {
            logger.LogTrace("Closing database connection");
            await database.CloseAsync();
            logger.LogInformation("Successfully closed the database connection");
            return true;
        }

        public bool Connect(IDatabaseConfiguration configuration)
        {
            if (database != null && database.DatabasePath == configuration.DatabasePath)
            {
                logger.LogWarning("Database already connected");
                return true;
            }

            logger.LogDebug("Using {@Database} database.", configuration.DatabasePath);
            var options = new SQLiteConnectionString(
                configuration.DatabasePath,
                true,
                key: configuration.EncryptionPassword,
                postKeyAction: p => p.Execute("PRAGMA cipher_compatibility = 3")
            );

            logger.LogTrace("Attempting to connect to the database.");
            database = new SQLiteAsyncConnection(options);
            logger.LogTrace("Successfully connected to the database.");
            return true;
        }

        public Task<T[]> GetAllAsync<T>() where T : IEntity, new()
        {
            logger.LogTrace("Obtaining all entities for table {@Table}", typeof(T).Name);
            return database.Table<T>().ToArrayAsync();
        }

        public async ValueTask DisposeAsync()
        {
            await database.CloseAsync();
            logger.LogDebug("Connection disposed.");
        }

        public bool IsEmpty()
        {
            return database.TableMappings.Any();
        }
    }
}
