using System;
using System.Threading.Tasks;
using BareTalk.Messenger.DataAccess.Configuration.Factory;
using BareTalk.Messenger.DataAccess.Entities;
using BareTalk.Messenger.Domain.Contracts.DataAccess;
using BareTalk.Messenger.Domain.Contracts.SecuredStorage;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.DataAccess.Connection.Builder
{
    public sealed class DatabaseConnectionBuilder : IDatabaseConnectionBuilder
    {
        private readonly IDatabaseConfigurationFactory databaseConfigurationFactory;
        private readonly ISecuredStorage applicationSecureStorage;
        private readonly ILogger<IDatabaseConnection> logger;

        private IDatabaseConnection? databaseConnection;

        public DatabaseConnectionBuilder(
            IDatabaseConfigurationFactory databaseConfigurationFactory,
            ISecuredStorage applicationSecureStorage,
            ILogger<IDatabaseConnection> logger
        )
        {
            this.databaseConfigurationFactory = databaseConfigurationFactory;
            this.applicationSecureStorage = applicationSecureStorage;
            this.logger = logger;
        }

        public async Task MigrateDatabaseAsync(bool forceMigrate = false)
        {
            if (databaseConnection == null)
            {
                throw new ArgumentNullException($"Please initialize the connection by calling \"{nameof(InitializeConnection)}\"", nameof(databaseConnection));
            }

            if (!forceMigrate)
            {
                logger.LogInformation("Skipping database migration");
                return;
            }

            logger.LogInformation("Applying migrations to database tables.");
            await databaseConnection.CreateTableAsync<Contact>();
            await databaseConnection.CreateTableAsync<KeyPair>();
            await databaseConnection.CreateTableAsync<Message>();
            await databaseConnection.CreateTableAsync<PrivateConversation>();
            await databaseConnection.CreateTableAsync<User>();
            logger.LogInformation("Successfully applied migrations to database tables.");
        }

        public async Task DropExistingDatabaseAsync()
        {
            if (databaseConnection == null)
            {
                throw new ArgumentNullException($"Please initialize the connection by calling \"{nameof(InitializeConnection)}\"", nameof(databaseConnection));
            }

            await databaseConnection.DropDatabaseAsync();
        }

        public IDatabaseConnection InitializeConnection()
        {
            logger.LogDebug("Building database connection");
            if (this.databaseConnection != null)
            {
                logger.LogWarning("Database connection already initialized");
                return this.databaseConnection;
            }

            IDatabaseConnection databaseConnection = new DatabaseConnection(logger);
            this.databaseConnection = new DatabaseConnectionErrorDecorator(databaseConnection, logger);
            logger.LogDebug("Database connection built successfully");

            return this.databaseConnection;
        }

        public bool ConnectWithCredentials(string username, string password)
        {
            if (databaseConnection == null)
            {
                throw new ArgumentNullException($"Please initialize the connection by calling \"{nameof(InitializeConnection)}\"", nameof(databaseConnection));
            }

            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentException("The provided username was null when attempting to initialize database connection.", nameof(username));
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("The provided password was null when attempting to initialize database connection.", nameof(password));
            }

            IDatabaseConfiguration configuration = databaseConfigurationFactory.Create(password, username);
            return databaseConnection.Connect(configuration);
        }

        public async ValueTask DisposeAsync()
        {
            if (databaseConnection != null)
            {
                await databaseConnection.DisposeAsync();
            }
        }

        public bool IsEmtpy()
        {
            return databaseConnection != null && databaseConnection.IsEmpty();
        }
    }
}
