using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.DataAccess;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.DataAccess.Connection
{
    public sealed class DatabaseConnectionErrorDecorator : IDatabaseConnection
    {
        private readonly IDatabaseConnection inner;
        private readonly ILogger<IDatabaseConnection> logger;

        public DatabaseConnectionErrorDecorator(
            IDatabaseConnection inner,
            ILogger<IDatabaseConnection> logger
        )
        {
            this.inner = inner;
            this.logger = logger;
        }

        public async Task<bool> CloseAsync()
        {
            try
            {
                return await inner.CloseAsync().ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                logger.LogError("Encountered error when closing database connection: Error: {@Error}", exception.Message);
            }

            return false;
        }

        public bool Connect(IDatabaseConfiguration configuration)
        {
            try
            {
                return inner.Connect(configuration);
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to connect to database {@Database}. Error: {@Error}", configuration.DatabasePath, exception.Message);
            }

            return false;
        }

        public async Task<bool> CreateTableAsync<T>() where T : IEntity, new()
        {
            try
            {
                return await inner.CreateTableAsync<T>().ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                logger.LogError("Encountered error when creating table {@Table}: Error: {@Error}", typeof(T).Name, exception.Message);
            }

            return false;
        }

        public async Task<bool> DeleteAsync<T>(T entity) where T : IEntity, new()
        {
            try
            {
                return await inner.DeleteAsync(entity).ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                logger.LogError("Encountered error when deleting entity {@Entity}: Error: {@Error}", typeof(T).Name, exception.Message);
            }

            return false;
        }

        public async ValueTask DisposeAsync()
        {
            try
            {
                await inner.DisposeAsync().ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                logger.LogError("Encountered error when disposing connection: Error: {@Error}", exception.Message);
            }
        }

        public async Task<bool> DropDatabaseAsync()
        {
            try
            {
                return await inner.DropDatabaseAsync().ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                logger.LogError("Encountered error when dropping database: Error: {@Error}", exception.Message);
            }

            return false;
        }

        public async Task<bool> DropTableAsync<T>() where T : IEntity, new()
        {
            try
            {
                return await inner.DropTableAsync<T>().ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                logger.LogError("Encountered error when dropping table {@Table}: Error: {@Error}", typeof(T).Name, exception.Message);
            }

            return false;
        }

        public async Task<IEnumerable<T>> FindAllAsync<T>(Expression<Func<T, bool>> predicate, Expression<Func<T, bool>>? orderBy = null) where T : IEntity, new()
        {
            try
            {
                return await inner.FindAllAsync(predicate, orderBy).ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                logger.LogError("Encountered error when executing find all on table {@Table}. Error: {@Error}", typeof(T).Name, exception.Message);
            }

            return new List<T>();
        }

        public async Task<T?> FindOneAsync<T>(Expression<Func<T, bool>> predicate) where T : class, IEntity, new()
        {
            try
            {
                return await inner.FindOneAsync(predicate).ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                logger.LogError("Encountered error when executing find one on table {@Table}. Error: {@Error}", typeof(T).Name, exception.Message);
            }

            return null;
        }

        public async Task<T[]> GetAllAsync<T>() where T : IEntity, new()
        {
            try
            {
                return await inner.GetAllAsync<T>();
            }
            catch (Exception exception)
            {
                logger.LogError("Encountered error when getting all elements of table {@Table}. Error: {@Error}", typeof(T).Name, exception.Message);
            }

            return new T[0];
        }

        public async Task<bool> InsertAsync<T>(T entity) where T : IEntity
        {
            try
            {
                return await inner.InsertAsync(entity).ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                logger.LogError("Encountered error when inserting entity in table {@Table}. Error: {@Error}", typeof(T).Name, exception.Message);
            }

            return false;
        }

        public bool IsEmpty()
        {
            try
            {
                return inner.IsEmpty();
            }
            catch (Exception exception)
            {
                logger.LogError("Encountered error when attempting to check whether the database is empty or not. Error: {@Error}", exception.Message);
            }

            return false;
        }

        public async Task<bool> UpdateAsync<T>(T entity) where T : IEntity, new()
        {
            try
            {
                return await inner.UpdateAsync(entity).ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                logger.LogError("Encountered error when updating entity in table {@Table}. Error: {@Error}", typeof(T).Name, exception.Message);
            }

            return false;
        }
    }
}
