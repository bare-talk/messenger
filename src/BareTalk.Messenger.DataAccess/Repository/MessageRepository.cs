using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.DataAccess;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.Repository;
using BareTalk.Messenger.Domain.ValueObjects;
using BareTalk.Messenger.Mapping.Mapper;
using DbMessage = BareTalk.Messenger.DataAccess.Entities.Message;

namespace BareTalk.Messenger.DataAccess.Repository
{
    public sealed class MessageRepository : IMessageRepository
    {
        private readonly IDatabaseConnection connection;
        private readonly IObjectMapper mapper;

        public MessageRepository(IDatabaseConnection connection, IObjectMapper mapper)
        {
            this.connection = connection;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<MessageModel>> GetFollowingMessagesAsync(Identifier conversationIdentifier, Identifier start)
        {
            IEnumerable<DbMessage> messages = await connection.FindAllAsync<DbMessage>(message => message.Id > start.Value && message.Id < start.Value + 10 && message.ConversationId == conversationIdentifier.Value);
            return messages.Any() ? mapper.Map<IEnumerable<DbMessage>, IEnumerable<MessageModel>>(messages) : new MessageModel[0];
        }

        public async Task<IEnumerable<MessageModel>> GetLatestMessagesAsync(Identifier conversationIdentifier)
        {
            IEnumerable<DbMessage> messages = await connection.FindAllAsync<DbMessage>(message => message.ConversationId == conversationIdentifier.Value);
            return messages.Any() ? mapper.Map<IEnumerable<DbMessage>, IEnumerable<MessageModel>>(messages) : new MessageModel[0];
        }

        public async Task<MessageModel?> SaveAsync(Identifier conversationIdentifier, Message messageContent, Username from)
        {
            var entity = new DbMessage
            {
                ConversationId = conversationIdentifier.Value,
                Content = messageContent.Value,
                From = from.Value
            };
            var created = await connection.InsertAsync(entity);

            return created ? mapper.Map<DbMessage, MessageModel>(entity) : null;
        }
    }
}
