using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BareTalk.Messenger.DataAccess.Entities;
using BareTalk.Messenger.Domain.Contracts.DataAccess;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.Repository;
using BareTalk.Messenger.Domain.ValueObjects;
using BareTalk.Messenger.Mapping.Mapper;

namespace BareTalk.Messenger.DataAccess.Repository
{
    public sealed class ContactRepository : IContactRepository
    {
        private readonly IDatabaseConnection databaseConnection;
        private readonly IObjectMapper mapper;

        public ContactRepository(IDatabaseConnection databaseConnection, IObjectMapper mapper)
        {
            this.databaseConnection = databaseConnection;
            this.mapper = mapper;
        }

        public async Task<ContactModel?> AddAsync(Username username, DisplayName displayName, Identifier keyPairIdentifier)
        {
            var contact = new Contact
            {
                Username = username.Value,
                DisplayName = displayName.Value,
                KeyPairId = keyPairIdentifier.Value
            };

            return await databaseConnection.InsertAsync(contact) ? mapper.Map<Contact, ContactModel>(contact) : null;
        }

        public async Task<ContactModel?> GetByIdAsync(Identifier identifier)
        {
            Contact? found = await databaseConnection.FindOneAsync<Contact>(contact => contact.Id == identifier.Value);
            return found != null ? mapper.Map<Contact, ContactModel>(found) : null;
        }

        public async Task<ContactModel?> GetByUsernameAsync(Username username)
        {
            Contact? found = await databaseConnection.FindOneAsync<Contact>(contact => contact.Username == username.Value);
            return found != null ? mapper.Map<Contact, ContactModel>(found) : null;
        }

        public async Task<IEnumerable<ContactModel>> GetExistingContactsAsync()
        {
            IEnumerable<Contact> contacts = await databaseConnection.FindAllAsync<Contact>(contact => contact.Connected);
            return contacts.Any() ? mapper.Map<IEnumerable<Contact>, IEnumerable<ContactModel>>(contacts) : new ContactModel[0];
        }

        public async Task<bool> MarkAsConnected(Identifier identifier)
        {
            Contact? found = await databaseConnection.FindOneAsync<Contact>(contact => contact.Id == identifier.Value);
            if (found == null)
            {
                return false;
            }

            found.Connected = true;
            return await databaseConnection.UpdateAsync(found);
        }
    }
}
