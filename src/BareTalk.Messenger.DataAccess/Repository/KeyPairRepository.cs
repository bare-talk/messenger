using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.DataAccess;
using BareTalk.Messenger.Domain.Repository;
using BareTalk.Messenger.Domain.ValueObjects;
using BareTalk.Messenger.Mapping.Mapper;
using DbKeyPair = BareTalk.Messenger.DataAccess.Entities.KeyPair;
using DomainKeyPair = BareTalk.Messenger.Domain.Entities.KeyPairModel;

namespace BareTalk.Messenger.DataAccess.Repository
{
    public class KeyPairRepository : IKeyPairRepository
    {
        private readonly IDatabaseConnection databaseConnection;
        private readonly IObjectMapper mapper;

        public KeyPairRepository(IDatabaseConnection databaseConnection, IObjectMapper mapper)
        {
            this.databaseConnection = databaseConnection;
            this.mapper = mapper;
        }

        public async Task<DomainKeyPair?> CreateAsync(PrivateKey privateKey, PublicKey publicKey)
        {
            var entity = new DbKeyPair
            {
                PrivateKey = privateKey.Value,
                PublicKey = publicKey.Value
            };

            var successful = await databaseConnection.InsertAsync(entity);
            return successful ? mapper.Map<DbKeyPair, DomainKeyPair>(entity) : null;
        }

        public async Task<DomainKeyPair?> FindByIdAsync(Identifier identifier)
        {
            DbKeyPair? foundKeyPair = await databaseConnection.FindOneAsync<DbKeyPair>(key => key.Id == identifier.Value);
            return foundKeyPair != null ? mapper.Map<DbKeyPair, DomainKeyPair>(foundKeyPair) : null;
        }

        public async Task<DomainKeyPair?> FindByPublicKeyAsync(PublicKey publicKey)
        {
            DbKeyPair? foundKeyPair = await databaseConnection.FindOneAsync<DbKeyPair>(key => key.PublicKey == publicKey.Value);
            return foundKeyPair != null ? mapper.Map<DbKeyPair, DomainKeyPair>(foundKeyPair) : null;
        }

        public Task<bool> UpdateAsync(DomainKeyPair keyPairModel)
        {
            var entity = new DbKeyPair
            {
                Id = keyPairModel.Identifier.Value,
                PrivateKey = keyPairModel.PrivateKey.Value,
                PublicKey = keyPairModel.PublicKey.Value
            };

            return databaseConnection.UpdateAsync(entity);
        }
    }
}
