using System.Threading.Tasks;
using BareTalk.Messenger.DataAccess.Entities;
using BareTalk.Messenger.Domain.Contracts.DataAccess;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.Repository;
using BareTalk.Messenger.Mapping.Mapper;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.DataAccess.Repository
{
    public sealed class UserRepository : IUserRepository
    {
        private readonly IDatabaseConnection databaseConnection;
        private readonly IObjectMapper mapper;
        private readonly ILogger<UserRepository> logger;

        public UserRepository(
            IDatabaseConnection databaseConnection,
            IObjectMapper mapper,
            ILogger<UserRepository> logger
        )
        {
            this.databaseConnection = databaseConnection;
            this.mapper = mapper;
            this.logger = logger;
        }

        public async Task<UserModel?> Create(
            string username,
            string displayName,
            string passwordSalt,
            string passwordHash
        )
        {
            var user = new User
            {
                Username = username,
                DisplayName = displayName,
                PasswordSalt = passwordSalt,
                PasswordHash = passwordHash
            };

            var createdSuccessfully = await databaseConnection.InsertAsync(user);
            if (!createdSuccessfully)
            {
                logger.LogError("Failed to insert user {@Username}", username);
                return null;
            }

            return mapper.Map<User, UserModel>(user);
        }

        public async Task<UserModel?> FindByUsernameAsync(string username)
        {
            User? user = await databaseConnection
                .FindOneAsync<User>(user => user.Username == username);

            return user != null ? mapper.Map<User, UserModel>(user) : null;
        }
    }
}
