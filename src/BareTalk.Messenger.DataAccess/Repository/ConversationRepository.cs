using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BareTalk.Messenger.DataAccess.Entities;
using BareTalk.Messenger.Domain.Contracts.DataAccess;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.Repository;
using BareTalk.Messenger.Domain.ValueObjects;
using BareTalk.Messenger.Mapping.Mapper;

namespace BareTalk.Messenger.DataAccess.Repository
{
    public sealed class ConversationRepository : IConversationRepository
    {
        private readonly IDatabaseConnection databaseConnection;
        private readonly IObjectMapper mapper;

        public ConversationRepository(IDatabaseConnection databaseConnection, IObjectMapper mapper)
        {
            this.databaseConnection = databaseConnection;
            this.mapper = mapper;
        }

        public async Task<PrivateConversationModel?> AddAsync(Identifier contactId)
        {
            var entity = new PrivateConversation()
            {
                ContactId = contactId.Value
            };

            var inserted = await databaseConnection.InsertAsync(entity);
            return inserted ? mapper.Map<PrivateConversation, PrivateConversationModel>(entity) : null;
        }

        public async Task<IEnumerable<PrivateConversationModel>> GetAllAsync()
        {
            IEnumerable<PrivateConversation> entities = await databaseConnection.GetAllAsync<PrivateConversation>();

            return entities.Any() ? mapper.Map<IEnumerable<PrivateConversation>, IEnumerable<PrivateConversationModel>>(entities) : new PrivateConversationModel[0];
        }

        public async Task<PrivateConversationModel?> GetByContactIdAsync(Identifier contactIdentifier)
        {
            PrivateConversation? entity = await databaseConnection.FindOneAsync<PrivateConversation>(c => c.ContactId == contactIdentifier.Value);

            return entity != null ? mapper.Map<PrivateConversation, PrivateConversationModel>(entity) : null;
        }

        public async Task<PrivateConversationModel?> GetByIdAsync(Identifier conversationId)
        {
            PrivateConversation? entity = await databaseConnection.FindOneAsync<PrivateConversation>(c => c.Id == conversationId.Value);

            return entity != null ? mapper.Map<PrivateConversation, PrivateConversationModel>(entity) : null;
        }
    }
}
