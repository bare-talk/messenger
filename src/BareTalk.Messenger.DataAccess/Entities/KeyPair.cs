using BareTalk.Messenger.Domain.Contracts.DataAccess;
using SQLite;

namespace BareTalk.Messenger.DataAccess.Entities
{
    internal sealed class KeyPair : IEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string PrivateKey { get; set; } = "";
        public string PublicKey { get; set; } = "";
    }
}
