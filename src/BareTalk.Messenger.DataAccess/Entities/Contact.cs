using BareTalk.Messenger.Domain.Contracts.DataAccess;
using SQLite;

namespace BareTalk.Messenger.DataAccess.Entities
{
    [Table("Contacts")]
    internal sealed class Contact : IEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Indexed]
        public string Username { get; set; } = "";

        public string DisplayName { get; set; } = "";

        public bool Connected { get; set; } = false;

        [Indexed]
        public int KeyPairId { get; set; }
    }
}
