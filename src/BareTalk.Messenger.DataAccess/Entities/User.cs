using BareTalk.Messenger.Domain.Contracts.DataAccess;
using SQLite;

namespace BareTalk.Messenger.DataAccess.Entities
{
    internal sealed class User : IEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string DisplayName { get; set; } = "";

        [Indexed]
        public string Username { get; set; } = "";

        public string PasswordSalt { get; set; } = "";
        public string PasswordHash { get; set; } = "";
    }
}
