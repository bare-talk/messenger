using BareTalk.Messenger.Domain.Contracts.DataAccess;
using SQLite;

namespace BareTalk.Messenger.DataAccess.Entities
{
    [Table("Messages")]
    internal sealed class Message : IEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Indexed]
        public int ConversationId { get; set; }

        public string Content { get; set; } = string.Empty;

        public string From { get; set; } = "";
    }
}
