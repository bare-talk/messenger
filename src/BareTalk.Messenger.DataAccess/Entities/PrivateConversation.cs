using BareTalk.Messenger.Domain.Contracts.DataAccess;
using SQLite;

namespace BareTalk.Messenger.DataAccess.Entities
{
    [Table("PrivateConversations")]
    internal sealed class PrivateConversation : IEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Indexed]
        public int ContactId { get; set; }
    }
}
