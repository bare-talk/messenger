using BareTalk.Messenger.Domain.Contracts.DataAccess;

namespace BareTalk.Messenger.DataAccess.Configuration.Factory
{
    public interface IDatabaseConfigurationFactory
    {
        IDatabaseConfiguration Create(string encryptionPassword, string ownerUsername);
    }
}
