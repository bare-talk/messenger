using BareTalk.Messenger.Domain.Contracts.DataAccess;

namespace BareTalk.Messenger.DataAccess.Configuration.Factory
{
    public sealed class DatabaseConfigurationFactory : IDatabaseConfigurationFactory
    {
        public IDatabaseConfiguration Create(string encryptionPassword, string ownerUsername)
        {
            return new DatabaseConfiguration(encryptionPassword, ownerUsername);
        }
    }
}
