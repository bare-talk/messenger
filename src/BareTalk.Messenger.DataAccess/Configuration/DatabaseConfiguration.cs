using System;
using System.IO;
using BareTalk.Messenger.Domain.Contracts.DataAccess;

namespace BareTalk.Messenger.DataAccess.Configuration
{
    internal sealed class DatabaseConfiguration : IDatabaseConfiguration
    {
        public string DatabasePath { get; }
        public string EncryptionPassword { get; }
        public string OwnerUsername { get; }

        public DatabaseConfiguration(string password, string ownerUsername)
        {
            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("Database password cannot be null or empty", nameof(password));
            }

            if (string.IsNullOrEmpty(ownerUsername))
            {
                throw new ArgumentException("Database owner cannot be null or empty", nameof(ownerUsername));
            }

            var databaseName = $"{ownerUsername}.db3";
            OwnerUsername = ownerUsername;
            DatabasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), databaseName);
            EncryptionPassword = password;
        }
    }
}
