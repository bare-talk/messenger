using BareTalk.Messenger.DataAccess.Entities;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;
using BareTalk.Messenger.Mapping.Factory;
using BareTalk.Messenger.Mapping.Mapper;
using Mapster;

namespace BareTalk.Messenger.DataAccess.Mapper
{
    public sealed class DataAccessMapperFactory : IMapperFactory
    {
        public IObjectMapper Create()
        {
            TypeAdapterConfig mapperConfiguration = CreateMappings();
            return new AdaptedMapper(mapperConfiguration);
        }

        private TypeAdapterConfig CreateMappings()
        {
            var configuration = new TypeAdapterConfig();

            configuration
                .ForType<Contact, ContactModel>()
                .ConstructUsing(
                    (src) => new ContactModel(
                        new Identifier(src.Id),
                        new Username(src.Username),
                        new DisplayName(src.DisplayName),
                        new Identifier(src.KeyPairId)
                    )
                );

            configuration
             .ForType<KeyPair, KeyPairModel>()
             .ConstructUsing(
                   src => new KeyPairModel(
                        new PrivateKey(src.PrivateKey),
                        new PublicKey(src.PublicKey),
                        new Identifier(src.Id)
                   )
              );

            configuration
                .ForType<Entities.Message, MessageModel>()
                .ConstructUsing(
                    src => new MessageModel(
                         new Identifier(src.Id),
                         new Identifier(src.ConversationId),
                         new Domain.ValueObjects.Message(src.Content),
                         new Username(src.From)
                    )
                );

            configuration
              .ForType<PrivateConversation, PrivateConversationModel>()
              .ConstructUsing(
                    src => new PrivateConversationModel(
                         new Identifier(src.Id),
                         new Identifier(src.ContactId)
                    )
               );

            configuration
               .ForType<User, UserModel>()
               .ConstructUsing(src => new UserModel(
                       new DisplayName(src.DisplayName),
                       new Username(src.Username),
                       new Identifier(src.Id),
                       new Salt(src.PasswordSalt),
                       new PasswordHash(src.PasswordHash)
                   )
               );

            return configuration;
        }
    }
}
