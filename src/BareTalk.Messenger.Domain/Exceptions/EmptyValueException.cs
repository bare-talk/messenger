using System;

namespace BareTalk.Messenger.Domain.Exceptions
{
    public sealed class EmptyValueException : Exception
    {
        public EmptyValueException(string fieldName, string className) : base($"Field {fieldName} was empty in {className}.")
        {
        }

        public EmptyValueException(string fieldName, string className, Exception innerException) : base($"Field {fieldName} was empty in {className}.", innerException)
        {
        }
    }
}
