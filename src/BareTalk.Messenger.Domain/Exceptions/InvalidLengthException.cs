using System;

namespace BareTalk.Messenger.Domain.Exceptions
{
    public sealed class InvalidLengthException : Exception
    {
        public InvalidLengthException(string fieldName, string className) : base($"Field {fieldName} length was invalid in {className}.")
        {
        }

        public InvalidLengthException(string fieldName, string className, Exception innerException) : base($"Field {fieldName} length was invalid in {className}.", innerException)
        {
        }
    }
}
