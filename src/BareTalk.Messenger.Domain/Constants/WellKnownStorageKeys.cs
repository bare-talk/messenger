namespace BareTalk.Messenger.Domain.Constants
{
    public static class WellKnownStorageKeys
    {
        public const string PasswordKey = "bare-talk.messenger.user-password.key";
        public const string UsernameKey = "bare-talk.messenger.username.key";
        public const string DatabaseAlreadySetup = "bare-talk.messenger.setup.key";
    }
}
