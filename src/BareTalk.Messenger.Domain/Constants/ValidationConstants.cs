namespace BareTalk.Messenger.Domain.Constants
{
    public sealed class ValidationConstants
    {
        public const int MinMessageLength = 1;
        public const int MaxMessageLength = 512;
        public const int MinPasswordLength = 8;
        public const int MaxPasswordLength = 28;
        public const int MinUsernameLength = 6;
        public const int MaxUsernameLength = 16;
        public const int MinDisplayNameLength = 6;
        public const int MaxDisplayNameLength = 16;
    }
}
