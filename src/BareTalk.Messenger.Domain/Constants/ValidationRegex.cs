namespace BareTalk.Messenger.Domain.Constants
{
    public static class ValidationRegexes
    {
        public const string UsernameRegex = "^[a-z0-9_]*$";
        public const string PasswordRegex = @"^[a-z0-9_\-!@#\$%\^&\*()=+`~\[\]|+=]*$";
        public const string DisplayNameRegex = "^[a-z0-9]*$";
    }
}
