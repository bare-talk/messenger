using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Repository
{
    public interface IMessageRepository
    {
        Task<IEnumerable<MessageModel>> GetLatestMessagesAsync(Identifier conversationIdentifier);

        Task<MessageModel?> SaveAsync(Identifier conversationIdentifier, Message messageContent, Username from);

        Task<IEnumerable<MessageModel>> GetFollowingMessagesAsync(Identifier conversationIdentifier, Identifier start);
    }
}
