using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Repository
{
    public interface IConversationRepository
    {
        Task<PrivateConversationModel?> GetByIdAsync(Identifier conversationId);

        Task<PrivateConversationModel?> AddAsync(Identifier identifier);

        Task<PrivateConversationModel?> GetByContactIdAsync(Identifier contactIdentifier);

        Task<IEnumerable<PrivateConversationModel>> GetAllAsync();
    }
}
