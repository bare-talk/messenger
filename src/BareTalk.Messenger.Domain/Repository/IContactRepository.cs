using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Repository
{
    public interface IContactRepository
    {
        Task<ContactModel?> GetByIdAsync(Identifier identifier);

        Task<ContactModel?> GetByUsernameAsync(Username username);

        Task<ContactModel?> AddAsync(Username username, DisplayName displayName, Identifier keyPairIdentifier);

        Task<IEnumerable<ContactModel>> GetExistingContactsAsync();

        Task<bool> MarkAsConnected(Identifier identifier);
    }
}
