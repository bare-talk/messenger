using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;

namespace BareTalk.Messenger.Domain.Repository
{
    public interface IUserRepository
    {
        Task<UserModel?> Create(string username, string displayName, string passwordSalt, string passwordHash);

        Task<UserModel?> FindByUsernameAsync(string username);
    }
}
