using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Repository
{
    public interface IKeyPairRepository
    {
        Task<KeyPairModel?> CreateAsync(PrivateKey privateKey, PublicKey publicKey);

        Task<KeyPairModel?> FindByIdAsync(Identifier identifier);

        Task<KeyPairModel?> FindByPublicKeyAsync(PublicKey publicKey);

        Task<bool> UpdateAsync(KeyPairModel keyPairModel);
    }
}
