using BareTalk.Messenger.Domain.Exceptions;

namespace BareTalk.Messenger.Domain.ValueObjects
{
    public sealed class Signature : AbstractValueObject<string>
    {
        public Signature(string value) : base(value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new EmptyValueException(nameof(value), nameof(Signature));
            }
        }
    }
}
