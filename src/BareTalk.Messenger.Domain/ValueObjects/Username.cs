using BareTalk.Messenger.Domain.Constants;
using BareTalk.Messenger.Domain.Exceptions;

namespace BareTalk.Messenger.Domain.ValueObjects
{
    public sealed class Username : AbstractValueObject<string>
    {
        public Username(string value) : base(value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new EmptyValueException(nameof(value), nameof(Username));
            }

            if (value.Length < ValidationConstants.MinUsernameLength || value.Length > ValidationConstants.MaxUsernameLength)
            {
                throw new InvalidLengthException(nameof(value), nameof(Username));
            }
        }
    }
}
