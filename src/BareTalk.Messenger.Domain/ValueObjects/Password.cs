using BareTalk.Messenger.Domain.Constants;
using BareTalk.Messenger.Domain.Exceptions;

namespace BareTalk.Messenger.Domain.ValueObjects
{
    public sealed class Password : AbstractValueObject<string>
    {
        public Password(string value) : base(value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new EmptyValueException(nameof(value), nameof(Password));
            }

            if (value.Length < ValidationConstants.MinPasswordLength || value.Length > ValidationConstants.MaxPasswordLength)
            {
                throw new InvalidLengthException(nameof(value), nameof(Password));
            }
        }
    }
}
