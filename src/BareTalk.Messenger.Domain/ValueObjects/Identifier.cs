using BareTalk.Messenger.Domain.Exceptions;

namespace BareTalk.Messenger.Domain.ValueObjects
{
    public sealed class Identifier : AbstractValueObject<int>
    {
        public Identifier(int value) : base(value)
        {
            if (value < 0)
            {
                throw new EmptyValueException(nameof(value), nameof(Identifier));
            }
        }

        public static Identifier Empty() => new Identifier(0);
    }
}
