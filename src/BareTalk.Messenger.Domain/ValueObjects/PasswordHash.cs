using BareTalk.Messenger.Domain.Exceptions;

namespace BareTalk.Messenger.Domain.ValueObjects
{
    public sealed class PasswordHash : AbstractValueObject<string>
    {
        public PasswordHash(string value) : base(value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new EmptyValueException(nameof(value), nameof(PasswordHash));
            }
        }
    }
}
