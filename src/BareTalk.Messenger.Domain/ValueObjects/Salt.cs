using BareTalk.Messenger.Domain.Exceptions;

namespace BareTalk.Messenger.Domain.ValueObjects
{
    public sealed class Salt : AbstractValueObject<string>
    {
        public Salt(string value) : base(value)
        {
            if (value.Length == 0)
            {
                throw new EmptyValueException(nameof(value), nameof(Salt));
            }
        }
    }
}
