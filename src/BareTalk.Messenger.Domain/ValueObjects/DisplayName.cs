using BareTalk.Messenger.Domain.Constants;
using BareTalk.Messenger.Domain.Exceptions;

namespace BareTalk.Messenger.Domain.ValueObjects
{
    public sealed class DisplayName : AbstractValueObject<string>
    {
        public DisplayName(string value) : base(value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new EmptyValueException(nameof(value), nameof(DisplayName));
            }

            if (value.Length < ValidationConstants.MinDisplayNameLength || value.Length > ValidationConstants.MaxDisplayNameLength)
            {
                throw new InvalidLengthException(nameof(value), nameof(DisplayName));
            }
        }
    }
}
