using BareTalk.Messenger.Domain.Constants;
using BareTalk.Messenger.Domain.Exceptions;

namespace BareTalk.Messenger.Domain.ValueObjects
{
    public sealed class Message : AbstractValueObject<string>
    {
        public Message(string value) : base(value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new EmptyValueException(nameof(value), nameof(Message));
            }

            if (value.Length < ValidationConstants.MinMessageLength || value.Length > ValidationConstants.MaxMessageLength)
            {
                throw new InvalidLengthException(nameof(value), nameof(Message));
            }
        }
    }
}
