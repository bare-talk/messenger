using BareTalk.Messenger.Domain.Exceptions;

namespace BareTalk.Messenger.Domain.ValueObjects
{
    public sealed class PrivateKey : AbstractValueObject<string>
    {
        public PrivateKey(string value) : base(value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new EmptyValueException(nameof(value), nameof(PrivateKey));
            }
        }
    }
}
