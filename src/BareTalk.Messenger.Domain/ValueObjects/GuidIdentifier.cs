using System;
using BareTalk.Messenger.Domain.Exceptions;

namespace BareTalk.Messenger.Domain.ValueObjects
{
    public sealed class GuidIdentifier : AbstractValueObject<Guid>
    {
        public GuidIdentifier(Guid value) : base(value)
        {
            if (value == default || value == Guid.Empty)
            {
                throw new EmptyValueException(nameof(value), nameof(GuidIdentifier));
            }
        }
    }
}
