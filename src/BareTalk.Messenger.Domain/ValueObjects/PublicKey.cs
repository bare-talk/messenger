using BareTalk.Messenger.Domain.Exceptions;

namespace BareTalk.Messenger.Domain.ValueObjects
{
    public sealed class PublicKey : AbstractValueObject<string>
    {
        public PublicKey(string value) : base(value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new EmptyValueException(nameof(value), nameof(ValueObjects.PublicKey));
            }
        }
    }
}
