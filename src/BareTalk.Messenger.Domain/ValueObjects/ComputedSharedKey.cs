using BareTalk.Messenger.Domain.Exceptions;

namespace BareTalk.Messenger.Domain.ValueObjects
{
    public sealed class ComputedSharedKey : AbstractValueObject<string>
    {
        public ComputedSharedKey(string value) : base(value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new EmptyValueException(nameof(value), nameof(ComputedSharedKey));
            }
        }
    }
}
