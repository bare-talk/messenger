namespace BareTalk.Messenger.Domain.ValueObjects
{
    public abstract class AbstractValueObject<T>
    {
        public T Value { get; private set; }

        protected AbstractValueObject(T value)
        {
            Value = value;
        }
    }
}
