namespace BareTalk.Messenger.Domain.Contracts.Encryption
{
    public interface ISignMessages
    {
        byte[] Sign(byte[] message, byte[] expandedPrivateKey);

        bool Verify(byte[] signature, byte[] message, byte[] publicKey);
    }
}
