namespace BareTalk.Messenger.Domain.Contracts.Encryption
{
    public interface IExchangeKeys
    {
        byte[] KeyExchange(byte[] publicKey, byte[] privateKey);
    }
}
