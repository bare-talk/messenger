using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Contracts.Encryption
{
    public interface IGenerateKeys
    {
        public (PrivateKey?, PublicKey?) Generate();
    }
}
