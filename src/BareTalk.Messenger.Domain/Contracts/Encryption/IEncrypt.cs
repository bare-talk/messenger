namespace BareTalk.Messenger.Domain.Contracts.Encryption
{
    public interface IEncrypt
    {
        byte[] Encrypt(string plainTextMessage, byte[] sharedKey);
    }
}
