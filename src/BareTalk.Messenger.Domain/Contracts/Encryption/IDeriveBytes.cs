namespace BareTalk.Messenger.Domain.Contracts.Encryption
{
    public interface IDeriveBytes
    {
        byte[] GetBytes(string password, byte[] salt);
    }
}
