namespace BareTalk.Messenger.Domain.Contracts.Encryption
{
    public interface IDecrypt
    {
        string Decrypt(byte[] encryptedMessageBytes, byte[] sharedKey);
    }
}
