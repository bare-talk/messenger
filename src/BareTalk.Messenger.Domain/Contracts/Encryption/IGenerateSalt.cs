namespace BareTalk.Messenger.Domain.Contracts.Encryption
{
    public interface IGenerateSalt
    {
        byte[] GenerateSecureSalt(int numberOfBytes = 32);
    }
}
