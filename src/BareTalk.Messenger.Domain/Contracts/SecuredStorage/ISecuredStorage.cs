using System.Threading.Tasks;

namespace BareTalk.Messenger.Domain.Contracts.SecuredStorage
{
    public interface ISecuredStorage
    {
        Task<string> GetAsync(string key);

        string Get(string key);

        Task SetAsync(string key, string value);

        bool Remove(string key);
    }
}
