namespace BareTalk.Messenger.Domain.Contracts.Encoder
{
    public interface IEncode
    {
        byte[] Encode(string data);
    }
}
