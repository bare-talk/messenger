namespace BareTalk.Messenger.Domain.Contracts.Encoder
{
    public interface IDecode
    {
        string Decode(byte[] data);
    }
}
