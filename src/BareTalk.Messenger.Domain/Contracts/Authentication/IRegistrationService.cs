using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Contracts.Authentication
{
    public interface IRegistrationService
    {
        Task<UserModel?> RegisterAsync(Username username, DisplayName displayName, Password passwordValueObject);

        Task<UserModel?> ExistsAsync(Username username);
    }
}
