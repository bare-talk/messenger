using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Contracts.Authentication
{
    public interface IAuthenticationService
    {
        Task<UserModel?> AuthenticateAsync(Username username, Password password);

        Task<bool> AuthenticateWithExistingCredentialsAsync();

        Task<UserModel?> GetAuthenticatedUser();

        Task<bool> LogOutAsync();
    }
}
