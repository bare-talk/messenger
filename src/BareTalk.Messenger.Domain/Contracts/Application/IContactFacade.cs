using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;

namespace BareTalk.Messenger.Domain.Contracts.Application
{
    public interface IContactFacade
    {
        Task<ContactModel?> CompleteConnectAsync(MyAcknowledgedConnectOfferModel offer);

        Task<ContactModel?> AcknowledgeIncomingConnectionOfferAsync(IncomingConnectOfferModel offer);

        Task<bool> OfferToConnectAsync(NewContactModel newContactRequest);

        Task<IEnumerable<ContactModel>> GetExistingContactsAsync();
    }
}
