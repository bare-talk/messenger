using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Contracts.Application
{
    public interface IMessageFacade
    {
        Task<IEnumerable<ConversationMessageModel>> GetLatestMessagesAsync(Identifier conversationId);

        Task<ConversationMessageModel?> DecryptAndSaveAsync(EncryptedMessageModel encryptedMessage);

        Task<ConversationMessageModel?> EncryptAndSendAsync(NewMessageModel newMessage);
    }
}
