using System.Threading.Tasks;

namespace BareTalk.Messenger.Domain.Contracts.Application
{
    public interface IUserRegistrationFacade
    {
        Task<bool> RegisterAsync(string username, string password, string displayName);
    }
}
