using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Contracts.Application
{
    public interface IConversationService
    {
        Task<PrivateConversationModel?> GetByContactIdAsync(Identifier contactIdentifier);

        Task<IEnumerable<PrivateConversationModel>> GetConversationsAsync();

        Task<PrivateConversationModel?> AddAsync(Identifier identifier);
    }
}
