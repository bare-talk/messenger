using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Contracts.Application
{
    public interface IMessagesService
    {
        Task<MessageModel?> SaveAsync(Identifier conversationIdentifier, Message messageContent, Username from);

        Task<IEnumerable<MessageModel>> GetLatestMessagesAsync(Identifier conversationIdentifier);

        Task<IEnumerable<MessageModel>> GetAsync(Identifier conversationIdentifier, Identifier start);
    }
}
