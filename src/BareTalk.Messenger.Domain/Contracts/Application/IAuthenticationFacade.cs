using System.Threading.Tasks;

namespace BareTalk.Messenger.Domain.Contracts.Application
{
    public interface IAuthenticationFacade
    {
        Task<bool> AuthenticateAsync(string username, string password);

        Task<bool> LogOutAsync();
    }
}
