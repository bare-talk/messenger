using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Contracts.Application
{
    public interface IContactService
    {
        Task<ContactModel?> GetByIdAsync(Identifier identifier);

        Task<ContactModel?> GetByUsernameAsync(Username username);

        Task<IEnumerable<ContactModel>> GetExistingContactsAsync();

        Task<IEnumerable<ContactModel>> GetAllContactsAsync();

        Task<ContactModel?> AddAsync(Username contactUsername, DisplayName contactDisplayName, Identifier contactKeyPairIdentifier);

        Task<bool> MarkAsConnectedAsync(Identifier identifier);

        Task<Username?> GetContactUsernameForConversationAsync(Identifier conversationId);
    }
}
