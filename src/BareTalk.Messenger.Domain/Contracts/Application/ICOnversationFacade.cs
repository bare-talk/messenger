using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Contracts.Application
{
    public interface IConversationFacade
    {
        Task<FriendlyPrivateConversationModel?> GetByContactIdAsync(Identifier contactId);
        Task<IEnumerable<FriendlyPrivateConversationModel>> GetConversationsAsync();
    }
}
