using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Contracts.Application
{
    public interface IKeyPairService
    {
        Task<KeyPairModel?> SavePartialPair(PublicKey publicKey);

        Task<bool> AttachPrivateKeyAsync(PublicKey myPublishedKey, PublicKey contactKey);

        Task<IEnumerable<KeyPairModel>> GenerateRangeAsync(UserModel user, int numberOfKeys = 5);

        Task<bool> PublishRangeAsync(IEnumerable<KeyPairModel> publicKeys, UserModel user);

        Task<KeyPairModel?> GetByIdAsync(Identifier identifier);
    }
}
