using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BareTalk.Messenger.Domain.Contracts.DataAccess
{
    public interface IDatabaseConnection : IAsyncDisposable
    {
        Task<bool> CreateTableAsync<T>() where T : IEntity, new();

        Task<bool> DropTableAsync<T>() where T : IEntity, new();

        Task<bool> DropDatabaseAsync();

        Task<bool> DeleteAsync<T>(T entity) where T : IEntity, new();

        Task<IEnumerable<T>> FindAllAsync<T>(Expression<Func<T, bool>> predicate, Expression<Func<T, bool>>? orderBy = null) where T : IEntity, new();

        Task<T?> FindOneAsync<T>(Expression<Func<T, bool>> predicate) where T : class, IEntity, new();

        Task<bool> InsertAsync<T>(T entity) where T : IEntity;

        Task<bool> UpdateAsync<T>(T entity) where T : IEntity, new();

        bool Connect(IDatabaseConfiguration configuration);

        Task<bool> CloseAsync();

        bool IsEmpty();

        Task<T[]> GetAllAsync<T>() where T : IEntity, new();
    }
}
