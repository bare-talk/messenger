using System;
using System.Threading.Tasks;

namespace BareTalk.Messenger.Domain.Contracts.DataAccess
{
    public interface IDatabaseConnectionBuilder : IAsyncDisposable
    {
        IDatabaseConnection InitializeConnection();

        Task MigrateDatabaseAsync(bool forceMigrate = false);

        Task DropExistingDatabaseAsync();

        bool ConnectWithCredentials(string username, string password);

        bool IsEmtpy();
    }
}
