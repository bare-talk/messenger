namespace BareTalk.Messenger.Domain.Contracts.DataAccess
{
    public interface IDatabaseConfiguration
    {
        string DatabasePath { get; }
        string EncryptionPassword { get; }
        string OwnerUsername { get; }
    }
}
