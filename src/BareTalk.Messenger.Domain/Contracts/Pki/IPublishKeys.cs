using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;

namespace BareTalk.Messenger.Domain.Contracts.Pki
{
    public interface IPublishKeys
    {
        Task<bool> PublishRangeAsync(IEnumerable<KeyPairModel> publicKeys, UserModel user);
    }
}
