using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Contracts.Pki
{
    public interface IClaimPublicKeys
    {
        Task<KeyPairModel?> ClaimKeyAsync(Username username);

        Task<IEnumerable<AvailableContactModel>> QueryAvailableKeysFor(Username me, Username username);
    }
}
