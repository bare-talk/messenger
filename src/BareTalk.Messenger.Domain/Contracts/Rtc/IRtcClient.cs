using System;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Contracts.Rtc
{
    public interface IRtcClient : IAsyncDisposable
    {
        Task StartAsync(Username username);

        Task StopAsync();

        Task<bool> OfferToConnectAsync(Username to, PublicKey claimedPublicKey, Username from, DisplayName myDisplayName);

        Task<bool> SendMessageAsync(Message encryptedMessage, Username from, Username to, Signature messageSignature);

        Task<bool> AcknowledgeConnectionAsync(Username from, Username to, PublicKey claimedPublicKey);

        #region Observables

        /// <summary>
        /// Triggered when we receive a new encrypted message from someone we already know.
        /// </summary>
        IObservable<EncryptedMessageModel> NewMessageReceived { get; }

        /// <summary>
        /// Triggered when we received an offer to connect from somebody we don't know.
        /// </summary>
        IObservable<IncomingConnectOfferModel> ContactOfferReceived { get; }

        /// <summary>
        /// Triggered when our connect offer has been acknowledged.
        /// </summary>
        IObservable<MyAcknowledgedConnectOfferModel> ContactOfferAcknowledged { get; }

        #endregion Observables
    }
}
