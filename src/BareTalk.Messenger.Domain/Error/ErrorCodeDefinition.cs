namespace BareTalk.Messenger.Domain.Error
{
    public enum ErrorCodeDefinition
    {
        AuthenticationFailed = 0,
        LogoutFailed = 1,
        UserRegistrationFailed = 2
    }
}
