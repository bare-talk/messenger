using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Entities
{
    public sealed class AvailableContactModel
    {
        public AvailableContactModel(
            GuidIdentifier identifier,
            Username owner,
            DisplayName displayName
        )
        {
            Identifier = identifier;
            Owner = owner;
            DisplayName = displayName;
        }

        public GuidIdentifier Identifier { get; }
        public Username Owner { get; }
        public DisplayName DisplayName { get; }
    }
}
