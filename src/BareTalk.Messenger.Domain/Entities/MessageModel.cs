using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Entities
{
    public sealed class MessageModel
    {
        public Identifier Identifier { get; }
        public Identifier ConversationIdentifier { get; }
        public Message Content { get; }
        public Username From { get; }

        public MessageModel(
            Identifier identifier,
            Identifier conversationIdentifier,
            Message messageContent,
            Username from
        )
        {
            Identifier = identifier;
            ConversationIdentifier = conversationIdentifier;
            Content = messageContent;
            From = from;
        }
    }
}
