using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Entities
{
    public class EncryptedMessageModel
    {
        public EncryptedMessageModel(Message encryptedMessage, Signature messageSignature, Username from)
        {
            EncryptedMessage = encryptedMessage;
            MessageSignature = messageSignature;
            From = from;
        }

        public Message EncryptedMessage { get; }
        public Signature MessageSignature { get; }
        public Username From { get; }
    }
}
