using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Entities
{
    public sealed class KeyPairModel
    {
        public KeyPairModel(PrivateKey privateKey, PublicKey publicKey, Identifier identifier)
        {
            PrivateKey = privateKey;
            PublicKey = publicKey;
            Identifier = identifier;
        }

        public Identifier Identifier { get; }
        public PrivateKey PrivateKey { get; private set; }
        public PublicKey PublicKey { get; }

        public void AttachPrivateKey(PrivateKey privateKey)
        {
            PrivateKey = privateKey;
        }
    }
}
