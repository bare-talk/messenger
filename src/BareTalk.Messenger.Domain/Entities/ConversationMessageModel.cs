using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Entities
{
    public sealed class ConversationMessageModel
    {
        public Identifier Identifier { get; }
        public Identifier ConversationIdentifier { get; }
        public Message Content { get; }
        public Username From { get; }
        public Username Me { get; }

        public ConversationMessageModel(
            Identifier identifier,
            Identifier conversationIdentifier,
            Message messageContent,
            Username from,
            Username me
        )
        {
            Identifier = identifier;
            ConversationIdentifier = conversationIdentifier;
            Content = messageContent;
            From = from;
            Me = me;
        }
    }
}
