using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Entities
{
    public sealed class NewMessageModel
    {
        public NewMessageModel(Message content, Identifier conversationIdentifier)
        {
            Content = content;
            ConversationIdentifier = conversationIdentifier;
        }

        public Message Content { get; }
        public Identifier ConversationIdentifier { get; }
    }
}
