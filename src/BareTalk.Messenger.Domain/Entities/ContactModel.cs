using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Entities
{
    public sealed class ContactModel
    {
        public ContactModel(
            Identifier identifier,
            Username username,
            DisplayName displayName,
            Identifier keyPairIdentifier
        )
        {
            Identifier = identifier;
            Username = username;
            DisplayName = displayName;
            KeyPairIdentifier = keyPairIdentifier;
        }

        public Identifier Identifier { get; }
        public Username Username { get; }
        public DisplayName DisplayName { get; }
        public Identifier KeyPairIdentifier { get; }
    }
}
