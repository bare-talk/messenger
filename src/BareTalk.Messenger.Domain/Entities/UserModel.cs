using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Entities
{
    public sealed class UserModel
    {
        public DisplayName DisplayName { get; }
        public Username Username { get; }
        public Identifier Identifier { get; }
        public Salt Salt { get; }
        public PasswordHash PasswordHash { get; }

        public UserModel(
            DisplayName displayName,
            Username username,
            Identifier identifier,
            Salt salt,
            PasswordHash passwordHash
        )
        {
            DisplayName = displayName;
            Username = username;
            Identifier = identifier;
            Salt = salt;
            PasswordHash = passwordHash;
        }

        public bool Is(Username username)
        {
            return username.Value == Username.Value;
        }
    }
}
