using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Entities
{
    public sealed class MyAcknowledgedConnectOfferModel
    {
        public MyAcknowledgedConnectOfferModel(Username from, PublicKey claimedPublicKey)
        {
            From = from;
            MyPublishedPublicKey = claimedPublicKey;
        }

        /// <summary>
        /// This is the name of the party who accepted our connection offer.
        /// </summary>
        public Username From { get; }

        /// <summary>
        /// This is our own key which we published during registration. This key has been claimed by the other party as an acknowledgment to our connection offer.
        /// </summary>
        public PublicKey MyPublishedPublicKey { get; }
    }
}
