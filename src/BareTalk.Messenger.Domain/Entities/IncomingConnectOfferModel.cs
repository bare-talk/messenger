using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Entities
{
    public sealed class IncomingConnectOfferModel
    {
        public IncomingConnectOfferModel(Username from, PublicKey myPublicKey, DisplayName theirDisplayName)
        {
            TheirUsername = from;
            MyPublicKey = myPublicKey;
            TheirDisplayName = theirDisplayName;
        }

        /// <summary>
        /// This is the name of the party who accepted our connection offer.
        /// </summary>
        public Username TheirUsername { get; }

        /// <summary>
        /// The display name of the contact who offered to connect with us
        /// </summary>
        public DisplayName TheirDisplayName { get; }

        /// <summary>
        /// A public key, owned by us, published sometime, for which we have a corresponding private key
        /// </summary>
        public PublicKey MyPublicKey { get; }
    }
}
