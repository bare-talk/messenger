using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Entities
{
    public sealed class FriendlyPrivateConversationModel
    {
        public FriendlyPrivateConversationModel(Identifier identifier, ContactModel contact)
        {
            Identifier = identifier;
            Contact = contact;
        }

        public Identifier Identifier { get; }
        public ContactModel Contact { get; }
    }
}
