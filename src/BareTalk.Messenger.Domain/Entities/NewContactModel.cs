using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Entities
{
    public sealed class NewContactModel
    {
        public NewContactModel(Username username, Identifier partialkeyPairIdentifier, DisplayName displayName, PublicKey partysClaimedKey)
        {
            Username = username;
            PartialkeyPairIdentifier = partialkeyPairIdentifier;
            DisplayName = displayName;
            PartysClaimedKey = partysClaimedKey;
        }

        public Username Username { get; }
        public Identifier PartialkeyPairIdentifier { get; }
        public PublicKey PartysClaimedKey { get; }
        public DisplayName DisplayName { get; }
    }
}
