using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.Domain.Entities
{
    public sealed class PrivateConversationModel
    {
        public PrivateConversationModel(Identifier identifier, Identifier contactIdentifier)
        {
            Identifier = identifier;
            ContactIdentifier = contactIdentifier;
        }

        public Identifier Identifier { get; }
        public Identifier ContactIdentifier { get; }
    }
}
