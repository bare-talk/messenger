using Plugin.ValidationRules.Interfaces;

namespace BareTalk.Messenger.Validation.Rules
{
    public sealed class StringLengthRule : IValidationRule<string>
    {
        public StringLengthRule(int minimumLength, int maximumLength, string validationMessage)
        {
            MinimumLength = minimumLength;
            MaximumLength = maximumLength;
            ValidationMessage = validationMessage;
        }

        public string ValidationMessage { get; set; } = "";
        public int MinimumLength { get; }
        public int MaximumLength { get; }

        public bool Check(string value)
        {
            return !string.IsNullOrEmpty(value)
                && value.Length > MinimumLength
                && value.Length < MaximumLength;
        }
    }
}
