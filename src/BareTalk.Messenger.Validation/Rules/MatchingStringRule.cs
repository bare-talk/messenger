using Plugin.ValidationRules.Interfaces;

namespace BareTalk.Messenger.Validation.Rules
{
    public sealed class MatchingStringRule : IValidationRule<string>
    {
        public MatchingStringRule(string stringToMatch, string validationMessage)
        {
            StringToMatch = stringToMatch;
            ValidationMessage = validationMessage;
        }

        public string StringToMatch { get; set; }
        public string ValidationMessage { get; set; } = "";

        public bool Check(string value)
        {
            return value == StringToMatch;
        }
    }
}
