using Plugin.ValidationRules.Interfaces;

namespace BareTalk.Messenger.Validation.Rules
{
    public sealed class StringNotEmptyRule : IValidationRule<string>
    {
        public string ValidationMessage { get; set; } = "";

        public bool Check(string value)
        {
            return string.IsNullOrEmpty(value);
        }
    }
}
