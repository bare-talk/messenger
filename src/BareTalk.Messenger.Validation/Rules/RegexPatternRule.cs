using System.Text.RegularExpressions;
using Plugin.ValidationRules.Interfaces;

namespace BareTalk.Messenger.Validation.Rules
{
    public sealed class StringRegexPatternRule : IValidationRule<string>
    {
        public StringRegexPatternRule(string regexCharacters, string validationMessage)
        {
            RegexPattern = regexCharacters;
            ValidationMessage = validationMessage;
        }

        public string RegexPattern { get; }
        public string ValidationMessage { get; set; } = "";

        public bool Check(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            var regex = new Regex(RegexPattern);
            return regex.Match(value).Success;
        }
    }
}
