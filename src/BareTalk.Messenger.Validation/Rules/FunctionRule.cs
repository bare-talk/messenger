using System;
using Plugin.ValidationRules.Interfaces;

namespace BareTalk.Messenger.Validation.Rules
{
    public sealed class FunctionRule<T> : IValidationRule<T>
    {
        private readonly Func<T, bool> ruleFunc;

        public FunctionRule(Func<T, bool> ruleFunc, string validationMessage)
        {
            this.ruleFunc = ruleFunc;
            ValidationMessage = validationMessage;
        }

        public string ValidationMessage { get; set; } = "";

        public bool Check(T value)
        {
            return ruleFunc(value);
        }
    }
}
