using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Domain.Contracts.Encryption;
using BareTalk.Messenger.Domain.Contracts.Pki;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.Repository;
using BareTalk.Messenger.Domain.ValueObjects;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Application.Services
{
    public sealed class KeyPairService : IKeyPairService
    {
        private readonly IKeyPairRepository keyPairRepository;
        private readonly ILogger<KeyPairService> logger;
        private readonly IPublishKeys keyPublisher;
        private readonly IGenerateKeys keyGenerator;

        public KeyPairService(
            IKeyPairRepository keyPairRepository,
            ILogger<KeyPairService> logger,
            IPublishKeys keyPublisher,
            IGenerateKeys keyGenerator
        )
        {
            this.keyPairRepository = keyPairRepository;
            this.logger = logger;
            this.keyPublisher = keyPublisher;
            this.keyGenerator = keyGenerator;
        }

        public async Task<KeyPairModel?> SavePartialPair(PublicKey publicKey)
        {
            KeyPairModel? newKeyPar = await keyPairRepository.CreateAsync(new PrivateKey("UNSET"), publicKey);
            if (newKeyPar != null)
            {
                logger.LogInformation("Successfully inserted a partial key pair. Public key {@Key}", publicKey.Value);
                return newKeyPar;
            }

            logger.LogInformation("Failed to insert a partial key pair.");
            return null;
        }

        public async Task<IEnumerable<KeyPairModel>> GenerateRangeAsync(UserModel user, int numberOfKeys = 5)
        {
            var results = new List<KeyPairModel>();
            for (var i = 0; i < numberOfKeys; i++)
            {
                (PrivateKey? privateKey, PublicKey? publicKey) = keyGenerator.Generate();
                if (privateKey == null || publicKey == null)
                {
                    logger.LogError("Aborting inserting keys to database.");
                    return new List<KeyPairModel>();
                }
                logger.LogTrace("Successfully inserted {@Private} and {@Public} to database", privateKey.Value, publicKey.Value);

                KeyPairModel? newKeyPair = await keyPairRepository.CreateAsync(privateKey, publicKey);

                if (newKeyPair == null)
                {
                    logger.LogError("Aborting key generation due to database error.");
                    return new List<KeyPairModel>();
                }
                logger.LogInformation("Successfully published {@Private} and {@Public} to PKI", privateKey.Value, publicKey.Value);

                results.Add(newKeyPair);
            }

            return results;
        }

        public async Task<KeyPairModel?> GetByIdAsync(Identifier identifier)
        {
            return await keyPairRepository.FindByIdAsync(identifier);
        }

        public Task<bool> PublishRangeAsync(IEnumerable<KeyPairModel> publicKeys, UserModel user)
        {
            return keyPublisher.PublishRangeAsync(publicKeys, user);
        }

        /// <summary>
        /// Obtains the private key linked to myPublishedKey and attaches it to the contact key.
        /// </summary>
        /// <param name="myPublishedKey"></param>
        /// <param name="contactKeyPair"></param>
        /// <returns></returns>
        public async Task<bool> AttachPrivateKeyAsync(PublicKey myPublishedKey, PublicKey contactKey)
        {
            logger.LogTrace("Attempting to find the key pair identified by the public key.");
            KeyPairModel? publishedKeyPair = await keyPairRepository.FindByPublicKeyAsync(myPublishedKey);
            if (publishedKeyPair == null)
            {
                logger.LogError("No key pair matches the given published public key.");
                return false;
            }

            logger.LogTrace("Attempting to find the key pair of the contact.");
            KeyPairModel? contactKeyPair = await keyPairRepository.FindByPublicKeyAsync(contactKey);
            if (contactKeyPair == null)
            {
                logger.LogError("No key pair matches the given requested contact public key.");
                return false;
            }

            logger.LogTrace("Attempting to attach our private key to the contact public key.");
            contactKeyPair.AttachPrivateKey(publishedKeyPair.PrivateKey);
            var succeeded = await keyPairRepository.UpdateAsync(contactKeyPair);
            if (!succeeded)
            {
                logger.LogError("Failed to set the private key for the contact.");
                return false;
            }

            return true;
        }
    }
}
