using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.Repository;
using BareTalk.Messenger.Domain.ValueObjects;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Application.Services
{
    public sealed class ContactService : IContactService
    {
        private readonly IContactRepository contactRepository;
        private readonly IConversationRepository conversationRepository;
        private readonly ILogger<ContactService> logger;

        public ContactService(
            IContactRepository contactRepository,
            ILogger<ContactService> logger,
            IConversationRepository conversationRepository
        )
        {
            this.contactRepository = contactRepository;
            this.logger = logger;
            this.conversationRepository = conversationRepository;
        }

        public async Task<ContactModel?> AddAsync(Username contactUsername, DisplayName contactDisplayName, Identifier contactKeyPairIdentifier)
        {
            ContactModel? createdContact = await contactRepository.AddAsync(contactUsername, contactDisplayName, contactKeyPairIdentifier);
            if (createdContact == null)
            {
                logger.LogError("Failed to add a new contact.");
                return null;
            }

            logger.LogInformation("Successfully added a new contact.");
            return createdContact;
        }

        public Task<IEnumerable<ContactModel>> GetAllContactsAsync()
        {
            throw new System.NotImplementedException();
        }

        public async Task<ContactModel?> GetByIdAsync(Identifier identifier)
        {
            ContactModel? found = await contactRepository.GetByIdAsync(identifier);
            if (found == null)
            {
                logger.LogError("No contact was found for identifier {@Id}", identifier.Value);
                return null;
            }

            logger.LogInformation("Successfully found the requested contact by id.");
            return found;
        }

        public async Task<ContactModel?> GetByUsernameAsync(Username username)
        {
            ContactModel? found = await contactRepository.GetByUsernameAsync(username);
            if (found == null)
            {
                logger.LogError("No contact was found for username {@Username}", username.Value);
                return null;
            }

            logger.LogInformation("Successfully found the requested contact by username.");
            return found;
        }

        public async Task<Username?> GetContactUsernameForConversationAsync(Identifier conversationId)
        {
            PrivateConversationModel? conversation = await conversationRepository.GetByIdAsync(conversationId);
            if (conversation == null)
            {
                logger.LogError("No conversation was found for id {@ConversationId} when attempting to get the username for conversation.", conversationId.Value);
                return null;
            }

            ContactModel? contact = await contactRepository.GetByIdAsync(conversation.ContactIdentifier);
            if (contact == null)
            {
                logger.LogError("No contact was found for id {@ContactId} when attempting to get the username for conversation.", conversation.ContactIdentifier);
                return null;
            }

            return contact.Username;
        }

        public async Task<IEnumerable<ContactModel>> GetExistingContactsAsync()
        {
            IEnumerable<ContactModel> contacts = await contactRepository.GetExistingContactsAsync();
            if (!contacts.Any())
            {
                logger.LogInformation("No current contacts were found.");
            }

            logger.LogInformation("Found {@Contacts} contacts.", contacts.Count());
            return contacts;
        }

        public async Task<bool> MarkAsConnectedAsync(Identifier identifier)
        {
            var marked = await contactRepository.MarkAsConnected(identifier);
            if (!marked)
            {
                logger.LogError("Failed to mark contact {@Contact} as connected", identifier.Value);
                return false;
            }

            logger.LogInformation("Successfully marked contact {@Contact} as connected", identifier.Value);
            return true;
        }
    }
}
