using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.Repository;
using BareTalk.Messenger.Domain.ValueObjects;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Application.Services
{
    public class MessagesService : IMessagesService
    {
        private readonly IMessageRepository messageRepository;
        private readonly ILogger<MessagesService> logger;

        public MessagesService(IMessageRepository messageRepository, ILogger<MessagesService> logger)
        {
            this.messageRepository = messageRepository;
            this.logger = logger;
        }

        public async Task<IEnumerable<MessageModel>> GetAsync(Identifier conversationIdentifier, Identifier start)
        {
            IEnumerable<MessageModel>? moreMessages = await messageRepository.GetFollowingMessagesAsync(conversationIdentifier, start);
            if (!moreMessages.Any())
            {
                logger.LogError("No more messages were found.");
            }

            logger.LogInformation("Successfully retrieved more messages the message.");
            return moreMessages;
        }

        public async Task<IEnumerable<MessageModel>> GetLatestMessagesAsync(Identifier conversationIdentifier)
        {
            IEnumerable<MessageModel> latestMessages = await messageRepository.GetLatestMessagesAsync(conversationIdentifier);
            if (!latestMessages.Any())
            {
                logger.LogDebug("No latest messages found for conversation {@Conversation}", conversationIdentifier.Value);
            }

            logger.LogInformation("Successfully retrieved the latest messages.");
            return latestMessages;
        }

        public async Task<MessageModel?> SaveAsync(Identifier conversationIdentifier, Message messageContent, Username from)
        {
            MessageModel? savedMessage = await messageRepository.SaveAsync(conversationIdentifier, messageContent, from);
            if (savedMessage == null)
            {
                logger.LogError("Failed to save the message.");
                return null;
            }

            logger.LogInformation("Successfully saved the message.");
            return savedMessage;
        }
    }
}
