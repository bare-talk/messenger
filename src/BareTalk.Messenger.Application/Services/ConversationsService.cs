using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.Repository;
using BareTalk.Messenger.Domain.ValueObjects;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Application.Services
{
    public sealed class ConversationsService : IConversationService
    {
        private readonly IConversationRepository conversationRepository;
        private readonly ILogger<ConversationsService> logger;

        public ConversationsService(IConversationRepository conversationRepository, ILogger<ConversationsService> logger)
        {
            this.conversationRepository = conversationRepository;
            this.logger = logger;
        }

        public async Task<PrivateConversationModel?> AddAsync(Identifier conversationIdentifier)
        {
            PrivateConversationModel? created = await conversationRepository.AddAsync(conversationIdentifier);
            if (created != null)
            {
                logger.LogDebug("A new conversation was created.");
                return created;
            }

            logger.LogError("The conversation was not created.");
            return null;
        }

        public async Task<PrivateConversationModel?> GetByContactIdAsync(Identifier contactIdentifier)
        {
            PrivateConversationModel? conversation = await conversationRepository.GetByContactIdAsync(contactIdentifier);

            if (conversation != null)
            {
                logger.LogDebug("Found one conversation matching contact ID {@ContacId}", contactIdentifier.Value);
                return conversation;
            }

            logger.LogError("No conversation matches contact ID {@ContacId}", contactIdentifier.Value);
            return null;
        }

        public async Task<IEnumerable<PrivateConversationModel>> GetConversationsAsync()
        {
            IEnumerable<PrivateConversationModel> conversations = await conversationRepository.GetAllAsync();

            if (conversations.Any())
            {
                logger.LogDebug("Found {@Conversations} conversations", conversations.Count());
                return conversations;
            }

            logger.LogWarning("No conversations were found.");
            return new PrivateConversationModel[0];
        }
    }
}
