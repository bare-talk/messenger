using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Domain.Contracts.Authentication;
using BareTalk.Messenger.Domain.Contracts.Pki;
using BareTalk.Messenger.Domain.Contracts.Rtc;
using BareTalk.Messenger.Domain.Entities;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Application.Facades.Contact
{
    public sealed class ContactFacade : IContactFacade
    {
        private readonly IRtcClient rtcClient;
        private readonly IAuthenticationService authenticationService;
        private readonly ILogger<ContactFacade> logger;
        private readonly IKeyPairService keyPairService;
        private readonly IContactService contactService;
        private readonly IConversationService conversationService;
        private readonly IClaimPublicKeys claimPublic;

        public ContactFacade(
            IRtcClient rtcClient,
            IAuthenticationService authenticationService,
            ILogger<ContactFacade> logger,
            IKeyPairService keyPairService,
            IContactService contactService,
            IClaimPublicKeys claimPublic,
            IConversationService conversationService
        )
        {
            this.rtcClient = rtcClient;
            this.authenticationService = authenticationService;
            this.logger = logger;
            this.keyPairService = keyPairService;
            this.contactService = contactService;
            this.claimPublic = claimPublic;
            this.conversationService = conversationService;
        }

        public async Task<bool> OfferToConnectAsync(NewContactModel newContactRequest)
        {
            UserModel? currentlyLoggedInUser = await authenticationService.GetAuthenticatedUser();
            if (currentlyLoggedInUser == null)
            {
                logger.LogError("Failed to connect to another user as the current user is null.");
                return false;
            }

            ContactModel? newContact = await contactService.AddAsync(newContactRequest.Username, newContactRequest.DisplayName, newContactRequest.PartialkeyPairIdentifier);
            if (newContact == null)
            {
                logger.LogError("Failed to add a new contact");
                return false;
            }

            PrivateConversationModel? conversation = await conversationService.AddAsync(newContact.Identifier);
            if (conversation == null)
            {
                logger.LogError("Failed to create a conversation for the new contact.");
                return false;
            }

            var connectionSucceeded = await rtcClient.OfferToConnectAsync(newContactRequest.Username, newContactRequest.PartysClaimedKey, currentlyLoggedInUser.Username, currentlyLoggedInUser.DisplayName);
            if (!connectionSucceeded)
            {
                logger.LogError("Failed to dispatch the connect offer.");
                return false;
            }

            return true;
        }

        public async Task<ContactModel?> CompleteConnectAsync(MyAcknowledgedConnectOfferModel offer)
        {
            // get the contact
            ContactModel? contact = await contactService.GetByUsernameAsync(offer.From);
            if (contact == null)
            {
                logger.LogError("No contact found for {@User}.", offer.From.Value);
                return null;
            }

            // the contact key pair
            KeyPairModel? contactKeys = await keyPairService.GetByIdAsync(contact.KeyPairIdentifier);
            if (contactKeys == null)
            {
                logger.LogError("The contact doest not have corresponding keys");
                return null;
            }

            // attach our private (linked to our public key) key to their public key
            var attached = await keyPairService.AttachPrivateKeyAsync(offer.MyPublishedPublicKey, contactKeys.PublicKey);
            if (!attached)
            {
                logger.LogError("Failed to attach the private key to the claimed public key.");
                return null;
            }

            // mark as connected
            var setConnected = await contactService.MarkAsConnectedAsync(contact.Identifier);
            if (!setConnected)
            {
                logger.LogError("Failed to mark contact as connected.");
                return null;
            }

            return contact;
        }

        public Task<IEnumerable<ContactModel>> GetExistingContactsAsync()
        {
            return contactService.GetExistingContactsAsync();
        }

        public async Task<ContactModel?> AcknowledgeIncomingConnectionOfferAsync(IncomingConnectOfferModel offer)
        {
            // claim their key and save it in our database
            KeyPairModel? theirKeyPair = await claimPublic.ClaimKeyAsync(offer.TheirUsername);
            if (theirKeyPair == null)
            {
                logger.LogError("Failed to claim a public key for the incoming offer.");
                return null;
            }

            // create a contact with their username, display name and the public key we claimed for them
            ContactModel? newContact = await contactService.AddAsync(offer.TheirUsername, offer.TheirDisplayName, theirKeyPair.Identifier);
            if (newContact == null)
            {
                logger.LogError("Failed to add a new contact when an offer was received.");
                return null;
            }

            // add a conversation and link it to that contact
            PrivateConversationModel? conversation = await conversationService.AddAsync(newContact.Identifier);
            if (conversation == null)
            {
                logger.LogError("Failed to create a conversation for the new contact.");
                return null;
            }

            // attach our private (linked to our public key) key to their public key
            var attached = await keyPairService.AttachPrivateKeyAsync(offer.MyPublicKey, theirKeyPair.PublicKey);
            if (!attached)
            {
                logger.LogError("Failed to attach the private key to the claimed public key.");
                return null;
            }

            // send ACK with the public key we claimed from them
            UserModel? currentlyLoggedInUser = await authenticationService.GetAuthenticatedUser();
            if (currentlyLoggedInUser == null)
            {
                logger.LogError("Failed to connect to another user as the current user is null.");
                return null;
            }

            var acknowledged = await rtcClient.AcknowledgeConnectionAsync(currentlyLoggedInUser.Username, offer.TheirUsername, theirKeyPair.PublicKey);
            if (!acknowledged)
            {
                logger.LogError("Failed to dispatch connect ACK to another user.");
                return null;
            }

            // mark as connected
            var setConnected = await contactService.MarkAsConnectedAsync(newContact.Identifier);
            if (!setConnected)
            {
                logger.LogError("Failed to mark contact as connected.");
                return null;
            }

            return newContact;
        }
    }
}
