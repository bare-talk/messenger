using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Domain.Contracts.Authentication;
using BareTalk.Messenger.Domain.Contracts.DataAccess;
using BareTalk.Messenger.Domain.Contracts.Rtc;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.Repository;
using BareTalk.Messenger.Domain.ValueObjects;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Application.Facades.Registration
{
    public sealed class UserRegistrationFacade : IUserRegistrationFacade
    {
        private readonly IRegistrationService registrationService;
        private readonly IKeyPairService keyPairService;
        private readonly ILogger<UserRegistrationFacade> logger;
        private readonly IDatabaseConnectionBuilder databaseConnectionBuilder;
        private readonly IUserRepository userRepository;
        private readonly IRtcClient rtcClient;

        public UserRegistrationFacade(
            IRegistrationService registrationService,
            IKeyPairService keyPairService,
            ILogger<UserRegistrationFacade> logger,
            IDatabaseConnectionBuilder databaseConnectionBuilder,
            IUserRepository userRepository,
            IRtcClient rtcClient
        )
        {
            this.registrationService = registrationService;
            this.keyPairService = keyPairService;
            this.logger = logger;
            this.databaseConnectionBuilder = databaseConnectionBuilder;
            this.userRepository = userRepository;
            this.rtcClient = rtcClient;
        }

        public async Task<bool> RegisterAsync(string username, string password, string displayName)
        {
            UserModel? newUser = await RegisterUserAsync(username, password, displayName);
            if (newUser != null && await GenerateAndPublishKeysAsync(newUser))
            {
                await Task.Run(() => rtcClient.StartAsync(newUser.Username));
                return true;
            }

            return false;
        }

        private async Task<UserModel?> RegisterUserAsync(string username, string password, string displayName)
        {
            var usernameObject = new Username(username);
            var passwordObject = new Password(password);
            var displayNameObject = new DisplayName(displayName);
            databaseConnectionBuilder.ConnectWithCredentials(username, password);
            if (!databaseConnectionBuilder.IsEmtpy())
            {
                logger.LogDebug("The database fro the given credentials wasn't empty. Checking user.");
                UserModel? user = await userRepository.FindByUsernameAsync(username);
                if (user != null)
                {
                    logger.LogError("Please log in.");
                    return null;
                }
            }

            await databaseConnectionBuilder.MigrateDatabaseAsync(true);

            UserModel? newlyCreatedUser = await registrationService.RegisterAsync(usernameObject, displayNameObject, passwordObject);

            if (newlyCreatedUser != null)
            {
                logger.LogInformation("The user {@User} was registered successfully.", displayName);
                return newlyCreatedUser;
            }

            logger.LogInformation("Failed to register user {@User}.", displayName);
            return null;
        }

        private async Task<bool> GenerateAndPublishKeysAsync(UserModel newUser)
        {
            IEnumerable<KeyPairModel> publicKeys = await keyPairService.GenerateRangeAsync(newUser, 10);
            var enumeratedKeys = publicKeys.ToList();
            if (enumeratedKeys.Count == 0)
            {
                logger.LogDebug("User registration workflow failed because the generation of public keys failed.");
                return false;
            }

            var publishingSucceeded = await keyPairService.PublishRangeAsync(enumeratedKeys, newUser);
            if (!publishingSucceeded)
            {
                logger.LogDebug("User registration workflow failed because the publishing of public keys failed.");
                return false;
            }

            return true;
        }
    }
}
