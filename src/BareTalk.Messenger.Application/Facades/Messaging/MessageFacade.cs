using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Constants;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Domain.Contracts.Encoder;
using BareTalk.Messenger.Domain.Contracts.Encryption;
using BareTalk.Messenger.Domain.Contracts.Rtc;
using BareTalk.Messenger.Domain.Contracts.SecuredStorage;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Application.Facades.Messaging
{
    public sealed class MessageFacade : IMessageFacade
    {
        private readonly ISignMessages messageSigner;
        private readonly IDecrypt decryptor;
        private readonly IEncrypt encryptor;
        private readonly IContactService contactService;
        private readonly IKeyPairService keyPairService;
        private readonly IExchangeKeys exchangeKeys;
        private readonly IEncode base64Encoder;
        private readonly IDecode base64Decoder;
        private readonly IMessagesService messageService;
        private readonly IRtcClient rtcClient;
        private readonly ISecuredStorage securedStorage;
        private readonly IMessagesService messagesService;
        private readonly IConversationService conversationService;
        private readonly ILogger<MessageFacade> logger;

        public MessageFacade(
            ISignMessages messageSigner,
            IDecrypt decryptor,
            IEncrypt encryptor,
            IContactService contactService,
            IKeyPairService keyPairService,
            IExchangeKeys exchangeKeys,
            IEncode base64Encoder,
            IDecode base64Decoder,
            IMessagesService messageService,
            IRtcClient rtcClient,
            ISecuredStorage securedStorage,
            ILogger<MessageFacade> logger,
            IMessagesService messagesService,
            IConversationService conversationService
        )
        {
            this.messageSigner = messageSigner;
            this.decryptor = decryptor;
            this.encryptor = encryptor;
            this.contactService = contactService;
            this.keyPairService = keyPairService;
            this.exchangeKeys = exchangeKeys;
            this.base64Encoder = base64Encoder;
            this.base64Decoder = base64Decoder;
            this.messageService = messageService;
            this.rtcClient = rtcClient;
            this.securedStorage = securedStorage;
            this.logger = logger;
            this.messagesService = messagesService;
            this.conversationService = conversationService;
        }

        public async Task<ConversationMessageModel?> DecryptAndSaveAsync(EncryptedMessageModel encryptedMessage)
        {
            // get he contact associated to the username from
            ContactModel? contact = await contactService.GetByUsernameAsync(encryptedMessage.From);
            if (contact == null)
            {
                logger.LogError("No contact found for username {@Username}", encryptedMessage.From.Value);
                return null;
            }

            // get the key pair linked to that contact
            KeyPairModel? keyPair = await keyPairService.GetByIdAsync(contact.KeyPairIdentifier);
            if (keyPair == null)
            {
                logger.LogError("No key pair found for ID {@Id}", contact.KeyPairIdentifier.Value);
                return null;
            }

            // TODO: consider caching
            // convert keys to bytes
            var publicKeyBytes = base64Encoder.Encode(keyPair.PublicKey.Value);
            var privateKeyBytes = base64Encoder.Encode(keyPair.PrivateKey.Value);
            var signatureByes = base64Encoder.Encode(encryptedMessage.MessageSignature.Value);
            var sharedKeyBytes = exchangeKeys.KeyExchange(publicKeyBytes, privateKeyBytes);

            // convert the encrypted message from base 64 string to bytes and verify the signature
            var messageBytes = base64Encoder.Encode(encryptedMessage.EncryptedMessage.Value);
            var signatureValid = messageSigner.Verify(signatureByes, messageBytes, publicKeyBytes);
            if (!signatureValid)
            {
                logger.LogError("Message signature could not be verified.");
                return null;
            }

            // decrypt the message
            var plainTextMessage = decryptor.Decrypt(messageBytes, sharedKeyBytes);

            // get the conversation
            PrivateConversationModel? conversation = await conversationService.GetByContactIdAsync(contact.Identifier);
            if (conversation == null)
            {
                logger.LogError("The conversation was not found for contact when saving the incoming message.");
                return null;
            }

            // save the message in db
            MessageModel? messageModel = await messageService.SaveAsync(conversation.Identifier, new Message(plainTextMessage), encryptedMessage.From);
            if (messageModel == null)
            {
                logger.LogError("Failed to save the message in database");
                return null;
            }

            // get the current user username
            var myUsername = securedStorage.Get(WellKnownStorageKeys.UsernameKey);
            if (string.IsNullOrEmpty(myUsername))
            {
                logger.LogError("Get latest messages query failed because the username obtained using secured storage was empty or null.");
                return null;
            }

            // convert to the appropariate model
            return new ConversationMessageModel(
                messageModel.Identifier,
                messageModel.ConversationIdentifier,
                messageModel.Content,
                encryptedMessage.From,
                new Username(myUsername)
            );
        }

        public async Task<ConversationMessageModel?> EncryptAndSendAsync(NewMessageModel newMessage)
        {
            // get the contact
            ContactModel? contact = await contactService.GetByIdAsync(newMessage.ConversationIdentifier);
            if (contact == null)
            {
                logger.LogError("No contact found for Id {@Id}", newMessage.ConversationIdentifier);
                return null;
            }

            // retrieve the key pair: our private key and their public key
            KeyPairModel? keyPair = await keyPairService.GetByIdAsync(contact.KeyPairIdentifier);
            if (keyPair == null)
            {
                logger.LogError("No key pair found for ID {@Id}", contact.KeyPairIdentifier.Value);
                return null;
            }

            // get the key bytes
            var publicKeyBytes = base64Encoder.Encode(keyPair.PublicKey.Value);
            var privateKeyBytes = base64Encoder.Encode(keyPair.PrivateKey.Value);

            // compute the shared key and encrypt the message
            var sharedKeyBytes = exchangeKeys.KeyExchange(publicKeyBytes, privateKeyBytes);
            var encryptedMessage = encryptor.Encrypt(newMessage.Content.Value, sharedKeyBytes);
            var messageSignature = messageSigner.Sign(encryptedMessage, privateKeyBytes);

            // convert the message and signature to base64 strings
            var base64EncryptedMessage = base64Decoder.Decode(encryptedMessage);
            var base64Signature = base64Decoder.Decode(messageSignature);

            // send the encrypted message
            var from = securedStorage.Get(WellKnownStorageKeys.UsernameKey);
            var sent = await rtcClient.SendMessageAsync(new Message(base64EncryptedMessage), new Username(from), new Username(contact.Username.Value), new Signature(base64Signature));
            if (!sent)
            {
                logger.LogError("Failed to send the encrypted message.");
                return null;
            }
            logger.LogInformation("Successfully sent the encrypted message.");

            // get our username
            var myUsername = securedStorage.Get(WellKnownStorageKeys.UsernameKey);
            if (string.IsNullOrEmpty(myUsername))
            {
                logger.LogError("Send message failed because the username obtained using secured storage was empty or null.");
                return null;
            }
            var me = new Username(myUsername);

            // get the conversation
            PrivateConversationModel? conversation = await conversationService.GetByContactIdAsync(contact.Identifier);
            if (conversation == null)
            {
                logger.LogError("The conversation was not found for contact when saving the incoming message.");
                return null;
            }

            // save in db
            MessageModel? saved = await messageService.SaveAsync(conversation.Identifier, newMessage.Content, me);
            if (saved == null)
            {
                logger.LogError("Failed to saved the chat message");
                return null;
            }

            return new ConversationMessageModel(saved.Identifier, conversation.Identifier, saved.Content, new Username(myUsername), me);
        }

        public async Task<IEnumerable<ConversationMessageModel>> GetLatestMessagesAsync(Identifier conversationIdentifier)
        {
            // get the latest messages
            IEnumerable<MessageModel> messages = await messagesService.GetLatestMessagesAsync(conversationIdentifier);
            if (!messages.Any())
            {
                logger.LogDebug("No message were found for latest message query.");
                return new ConversationMessageModel[0];
            }

            // get my username
            var myUsername = securedStorage.Get(WellKnownStorageKeys.UsernameKey);
            if (string.IsNullOrEmpty(myUsername))
            {
                logger.LogError("Get latest messages query failed because the username obtained using secured storage was empty or null.");
                return new ConversationMessageModel[0];
            }

            //// get the contact username
            //Username? contactUsername = await contactService.GetContactUsernameForConversationAsync(conversationIdentifier);
            //if (contactUsername == null)
            //{
            //    logger.LogError("Get latest messages query failed because we failed to retrieve the contact username.");
            //    return new ConversationMessageModel[0];
            //}

            logger.LogInformation("Successfully retrieved the latest messages, proceeding to mapping.");
            return messages.Select(m => new ConversationMessageModel(
                 m.ConversationIdentifier,
                 m.ConversationIdentifier,
                 m.Content,
                 m.From,
                 new Username(myUsername)
            ));
        }
    }
}
