using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Domain.Contracts.Authentication;
using BareTalk.Messenger.Domain.Contracts.DataAccess;
using BareTalk.Messenger.Domain.Contracts.Rtc;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Application.Facades.Authentication
{
    public class AuthenticationFacade : IAuthenticationFacade
    {
        private readonly IAuthenticationService authenticationService;
        private readonly ILogger<AuthenticationFacade> logger;
        private readonly IRtcClient rtcClient;
        private readonly IDatabaseConnection databaseConnection;

        public AuthenticationFacade(
            IAuthenticationService authenticationService,
            ILogger<AuthenticationFacade> logger,
            IRtcClient rtcClient,
            IDatabaseConnection databaseConnection
        )
        {
            this.authenticationService = authenticationService;
            this.logger = logger;
            this.rtcClient = rtcClient;
            this.databaseConnection = databaseConnection;
        }

        public async Task<bool> AuthenticateAsync(string username, string password)
        {
            var usernameObject = new Username(username);
            UserModel? successfullyAuthenticated = await authenticationService.AuthenticateAsync(usernameObject, new Password(password));

            if (successfullyAuthenticated != null)
            {
                logger.LogInformation("Authentication successful.");
                await rtcClient.StartAsync(usernameObject);
                return true;
            }

            logger.LogError("Authentication failed.");
            return false;
        }

        public async Task<bool> LogOutAsync()
        {
            var loggedOutSuccessfully = await authenticationService.LogOutAsync();
            if (loggedOutSuccessfully)
            {
                logger.LogInformation("Logging out succeeded.");
                await rtcClient.StopAsync();
                await databaseConnection.CloseAsync();
                return true;
            }

            logger.LogError("Failed to log out.");
            return false;
        }
    }
}
