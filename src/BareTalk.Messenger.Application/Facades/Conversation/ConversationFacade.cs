using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Application.Facades.Conversation
{
    public sealed class ConversationFacade : IConversationFacade
    {
        private readonly IConversationService conversationService;
        private readonly IContactService contactService;
        private readonly ILogger<ConversationFacade> logger;

        public ConversationFacade(
            IConversationService conversationService,
            IContactService contactService,
            ILogger<ConversationFacade> logger
        )
        {
            this.conversationService = conversationService;
            this.contactService = contactService;
            this.logger = logger;
        }

        public async Task<IEnumerable<FriendlyPrivateConversationModel>> GetConversationsAsync()
        {
            IEnumerable<PrivateConversationModel>? conversations = await conversationService.GetConversationsAsync();
            if (!conversations.Any())
            {
                logger.LogError("Skipping mapping as no conversations  were found.");
                return new FriendlyPrivateConversationModel[0];
            }

            var results = new List<FriendlyPrivateConversationModel>();
            foreach (PrivateConversationModel? conversation in conversations)
            {
                ContactModel? contact = await contactService.GetByIdAsync(conversation.ContactIdentifier);
                if (contact != null)
                {
                    results.Add(new FriendlyPrivateConversationModel(conversation.Identifier, contact));
                }
            }

            return results;
        }

        public async Task<FriendlyPrivateConversationModel?> GetByContactIdAsync(Identifier contactId)
        {
            PrivateConversationModel? conversation = await conversationService.GetByContactIdAsync(contactId);
            if (conversation == null)
            {
                logger.LogError("No conversation was found during get by id in conversation facade.");
                return null;
            }

            ContactModel? contact = await contactService.GetByIdAsync(contactId);
            if (contact == null)
            {
                logger.LogError("No contact was found during get by id in conversation facade.");
                return null;
            }

            return new FriendlyPrivateConversationModel(
                conversation.Identifier,
                contact
            );
        }
    }
}
