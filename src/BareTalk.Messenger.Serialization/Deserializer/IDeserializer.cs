namespace BareTalk.Messenger.Serialization.Deserializer
{
    public interface IDeserializer
    {
        T? Deserialize<T>(string content) where T : class, IAmDeSerializable;
    }
}
