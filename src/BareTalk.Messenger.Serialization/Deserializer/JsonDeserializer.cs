using System;
using System.Text.Json;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Serialization.Deserializer
{
    public sealed class JsonDeserializer : IDeserializer
    {
        private readonly ILogger<JsonDeserializer> logger;

        public JsonDeserializer(ILogger<JsonDeserializer> logger)
        {
            this.logger = logger;
        }

        public T? Deserialize<T>(string json) where T : class, IAmDeSerializable
        {
            var opts = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            };

            try
            {
                return JsonSerializer.Deserialize<T>(json, opts);
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to deserialize content to {@Type}. Error: {@Error}", typeof(T).Name, exception.Message);
            }

            return null;
        }
    }
}
