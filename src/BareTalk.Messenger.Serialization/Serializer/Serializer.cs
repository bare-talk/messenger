using System;
using System.Text.Json;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Serialization.Serializer
{
    public sealed class Serializer : ISerializer
    {
        private readonly ILogger<Serializer> logger;

        public Serializer(ILogger<Serializer> logger)
        {
            this.logger = logger;
        }

        public string Serialize<T>(T item) where T : class, IAmSerializable
        {
            var opts = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            };

            try
            {
                return JsonSerializer.Serialize(item, opts);
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to serialize {@Item}. Error: {@Error}", typeof(T).Name, exception.Message);
            }

            return string.Empty;
        }
    }
}
