namespace BareTalk.Messenger.Serialization.Serializer
{
    public interface ISerializer
    {
        string Serialize<T>(T item) where T : class, IAmSerializable;
    }
}
