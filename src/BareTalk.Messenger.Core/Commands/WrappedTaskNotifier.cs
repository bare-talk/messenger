using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.ViewModels;

namespace BareTalk.Messenger.Core.Commands
{
    internal static class WrappedTaskNotifier
    {
        private static readonly ILogger<App> Logger = Mvx.IoCProvider.Resolve<ILogger<App>>();

        public static MvxNotifyTask Create(Func<Task> task, Action<Exception>? onException)
        {
            return MvxNotifyTask.Create(
                async () =>
                {
                    try
                    {
                        await task.Invoke();
                    }
                    catch (Exception exception)
                    {
                        Logger.LogError("Observed task exception: {@Message}", exception.Message);
                        onException?.Invoke(exception);
                    }
                });
        }
    }
}
