using System;
using System.Threading;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.ViewModels;

namespace BareTalk.Messenger.Core.Commands.Factory
{
    public interface ICommandFactory
    {
        IMvxCommand Create(Action action, Func<bool>? canExecute = null);

        IMvxCommand<T> Create<T>(Action<T> execute, Func<T, bool>? canExecute = null);

        IMvxAsyncCommand CreateAsyncCommand(Func<CancellationToken, Task> execute, Func<bool> canExecute, bool allowConcurrentExecutions = false);

        MvxNotifyTask CreateObserverFor(Func<Task> action, Action<Exception>? onException = null);
    }
}
