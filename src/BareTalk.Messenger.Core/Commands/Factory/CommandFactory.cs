using System;
using System.Threading;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.ViewModels;

namespace BareTalk.Messenger.Core.Commands.Factory
{
    internal sealed class CommandFactory : ICommandFactory
    {
        public IMvxCommand Create(Action action, Func<bool>? canExecute = null)
        {
            return new MvxCommand(action, canExecute);
        }

        public IMvxCommand<T> Create<T>(Action<T> execute, Func<T, bool>? canExecute = null)
        {
            return new MvxCommand<T>(execute, canExecute);
        }

        public IMvxAsyncCommand CreateAsyncCommand(Func<CancellationToken, Task> execute, Func<bool> canExecute, bool allowConcurrentExecutions = false)
        {
            return new MvxAsyncCommand(execute, canExecute, allowConcurrentExecutions);
        }

        public IMvxAsyncCommand<T> CreateAsyncCommand<T>(Func<T, Task> execute, Func<T, bool> canExecute, bool allowConcurrentExecutions = false)
        {
            return new MvxAsyncCommand<T>(execute, canExecute, allowConcurrentExecutions);
        }

        public MvxNotifyTask CreateObserverFor(Func<Task> action, Action<Exception>? onException = null)
        {
            return WrappedTaskNotifier.Create(action, onException);
        }
    }
}
