using System.Threading.Tasks;
using BareTalk.Messenger.Core.Commands.Factory;
using BareTalk.Messenger.Core.ViewModels.Landing;
using BareTalk.Messenger.Domain.Constants;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Validation.Rules;
using Microsoft.Extensions.Logging;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using Plugin.ValidationRules;

namespace BareTalk.Messenger.Core.ViewModels.Register
{
    public class RegisterViewModel : BaseViewModel
    {
        private readonly IUserRegistrationFacade registrationFacade;
        private readonly IAuthenticationFacade authenticationFacade;
        private readonly IMvxNavigationService navigationService;

        public IMvxCommand RegisterCommand { get; }

        private MvxNotifyTask registering = MvxNotifyTask.Create(Task.CompletedTask);

        public MvxNotifyTask Registering
        {
            get => registering;
            private set
            {
                registering = value;
                RaisePropertyChanged(nameof(Registering));
            }
        }

        public Validatable<string> Username { get; set; }
        public Validatable<string> Password { get; set; }
        public Validatable<string> ConfirmPassword { get; set; }
        public Validatable<string> DisplayName { get; set; }
        public ValidationUnit FormValidationStatus { get; set; }

        public RegisterViewModel(
            ICommandFactory commandFactory,
            ILogger<BaseViewModel> logger,
            IUserRegistrationFacade registrationFacade,
            IAuthenticationFacade authenticationFacade,
            IMvxNavigationService navigationService
        ) : base(commandFactory, logger)
        {
            Username = new Validatable<string>();
            Password = new Validatable<string>();
            ConfirmPassword = new Validatable<string>();
            DisplayName = new Validatable<string>();
            AddValidationRules();
            FormValidationStatus = new ValidationUnit(Username, Password, ConfirmPassword, DisplayName)
            {
                IsValid = false
            };

            RegisterCommand = commandFactory.Create(() => Registering = commandFactory.CreateObserverFor(() => RegisterAsync()), () => true);
            this.registrationFacade = registrationFacade;
            this.authenticationFacade = authenticationFacade;
            this.navigationService = navigationService;
        }

        private void AddValidationRules()
        {
            // username
            Username.Validations.Add(new StringLengthRule(ValidationConstants.MinUsernameLength, ValidationConstants.MaxUsernameLength, $"The username needs to be between {ValidationConstants.MinUsernameLength} and {ValidationConstants.MaxUsernameLength} characters long."));
            Username.Validations.Add(new StringRegexPatternRule(ValidationRegexes.UsernameRegex, "The username accepts only a-z, 0-9, and _ characters."));

            // display name
            DisplayName.Validations.Add(new StringLengthRule(ValidationConstants.MinDisplayNameLength, ValidationConstants.MaxDisplayNameLength, $"The display name needs to be between {ValidationConstants.MinDisplayNameLength} and {ValidationConstants.MaxDisplayNameLength} characters long."));
            DisplayName.Validations.Add(new StringRegexPatternRule(ValidationRegexes.DisplayNameRegex, "The display name only accepts a-z and 0-9 characters."));

            // password
            Password.Validations.Add(new StringLengthRule(ValidationConstants.MinPasswordLength, ValidationConstants.MaxPasswordLength, $"The password needs to be between {ValidationConstants.MinPasswordLength} and {ValidationConstants.MaxPasswordLength} characters long."));
            Password.Validations.Add(new StringRegexPatternRule(ValidationRegexes.PasswordRegex, "The password does not contain valid characters. Allowed characters are a-z, 0-9, !@#$%^&*()_+=|[]`~"));

            // confirm password
            Password.Validations.Add(new FunctionRule<string>((value) => value == Password.Value, "The password do not match"));
        }

        private async Task RegisterAsync()
        {
            var successfullyRegistered = await registrationFacade.RegisterAsync(Username.Value, Password.Value, DisplayName.Value);
            if (successfullyRegistered)
            {
                var successfullySignedIn = await authenticationFacade.AuthenticateAsync(Username.Value, Password.Value);
                if (successfullySignedIn)
                {
                    await navigationService.Navigate<LandingPageViewModel>();
                }
            }
        }
    }
}
