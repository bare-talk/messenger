using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Core.Commands.Factory;
using BareTalk.Messenger.Core.ViewModels.Contact;
using BareTalk.Messenger.Core.ViewModels.Conversation;
using BareTalk.Messenger.Core.ViewModels.Settings;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Messenger.Messages;
using Microsoft.Extensions.Logging;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.Plugin.Messenger;
using MvvmCross.ViewModels;

namespace BareTalk.Messenger.Core.ViewModels.Landing
{
    public sealed class ConversationsTabViewModel : BaseViewModel
    {
        #region Commands
        public IMvxCommand NavigateToContactsQueryPage { get; }
        public IMvxCommand<FriendlyPrivateConversationModel> NavigateToConversationCommand { get; }
        public IMvxAsyncCommand NavigateToSettingsCommand { get; }

        #endregion Commands

        #region Notifiers
        public MvxNotifyTask? AddContactNotifier { get; private set; }
        public MvxNotifyTask? NavigateToConversationNotifier { get; private set; }
        public MvxNotifyTask? LoadingConversationsNotifier { get; private set; }
        #endregion Notifiers

        #region Data
        public MvxObservableCollection<FriendlyPrivateConversationModel> Conversations { get; set; } = new MvxObservableCollection<FriendlyPrivateConversationModel>();
        #endregion Data

        public ConversationsTabViewModel(
            ICommandFactory commandFactory,
            ILogger<BaseViewModel> logger,
            IMvxNavigationService navigationService,
            IContactFacade contactConnectionFacade,
            IConversationFacade conversationFacade,
            IMvxMessenger messenger
        ) : base(commandFactory, logger)
        {
            this.navigationService = navigationService;
            this.contactConnectionFacade = contactConnectionFacade;
            this.conversationFacade = conversationFacade;
            this.messenger = messenger;
            this.logger = logger;

            NavigateToContactsQueryPage = commandFactory.Create(
                () => AddContactNotifier = commandFactory.CreateObserverFor(AddContactCommandAsync),
                () => true
            );

            NavigateToConversationCommand = commandFactory.Create<FriendlyPrivateConversationModel>(
                (contact) => NavigateToConversationNotifier = commandFactory.CreateObserverFor(() => NavigateToConversationAsync(contact)),
                (contact) => contact != null
            );

            NavigateToSettingsCommand = commandFactory.CreateAsyncCommand((_) => NavigateToSettingsPageAsync(), () => true);
        }

        private readonly IMvxNavigationService navigationService;
        private readonly IContactFacade contactConnectionFacade;
        private readonly IConversationFacade conversationFacade;
        private readonly IMvxMessenger messenger;
        private readonly ILogger<BaseViewModel> logger;

        /// <summary>
        /// Keep the subscription tokens to avoid loss of subscription
        /// </summary>
        private MvxSubscriptionToken? connectedSubscription;

        private MvxSubscriptionToken? connectAckSubscription;

        public override Task Initialize()
        {
            LoadingConversationsNotifier = CommandFactory.CreateObserverFor(LoadConversationsAsync);

            connectedSubscription = messenger.SubscribeOnMainThread<ConnectOfferAcknowledgedMessage>(
                async (notification) =>
                {
                    logger.LogDebug("Conversations view model got a new contact offer acknowledged message.");
                    FriendlyPrivateConversationModel? conversation = await conversationFacade.GetByContactIdAsync(notification.Contact.Identifier);

                    if (conversation != null)
                    {
                        Conversations.Add(conversation);
                    }
                });

            connectAckSubscription = messenger.SubscribeOnMainThread<MyAcknowledgedConnectOfferMessage>(async (notification) =>
            {
                logger.LogDebug("Conversations view model got a new MY contact offer got acknowledged message.");
                FriendlyPrivateConversationModel? conversation = await conversationFacade.GetByContactIdAsync(notification.Contact.Identifier);

                if (conversation != null)
                {
                    Conversations.Add(conversation);
                }
            });

            return base.Initialize();
        }

        private async Task NavigateToConversationAsync(FriendlyPrivateConversationModel conversation)
        {
            await navigationService.Navigate<ConversationViewModel, FriendlyPrivateConversationModel>(conversation);
        }

        private async Task AddContactCommandAsync()
        {
            NewContactModel? newContact = await navigationService.Navigate<ContactViewModel, NewContactModel>();
            if (newContact != null)
            {
                AddContactNotifier = CommandFactory.CreateObserverFor(async () =>
                {
                    logger.LogInformation("Attempting to dispatch offer to connect to {@User}", newContact.DisplayName);
                    var connected = await contactConnectionFacade.OfferToConnectAsync(newContact);
                    if (!connected)
                    {
                        logger.LogError("Connecting to contact {@Contact} failed", newContact.Username.Value);
                    }
                });

                return;
            }

            logger.LogInformation("The provided contact was null, aborting connect.");
        }

        private async Task LoadConversationsAsync()
        {
            logger.LogTrace("Initializing conversations.");
            IEnumerable<FriendlyPrivateConversationModel> conversations = await conversationFacade.GetConversationsAsync();
            Conversations.AddRange(conversations);
            logger.LogInformation("Successfully initialized conversations.");
        }

        private async Task NavigateToSettingsPageAsync()
        {
            await navigationService.Navigate<SettingsViewModel>();
        }
    }
}
