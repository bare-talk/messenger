using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BareTalk.Messenger.Core.Commands.Factory;
using BareTalk.Messenger.Core.ViewModels.Settings;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Messenger.Messages;
using Microsoft.Extensions.Logging;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.Plugin.Messenger;
using MvvmCross.ViewModels;

namespace BareTalk.Messenger.Core.ViewModels.Landing
{
    public sealed class ContactsTabViewModel : BaseViewModel
    {
        public IMvxAsyncCommand NavigateToSettingsCommand { get; }
        public MvxObservableCollection<ContactModel> Contacts { get; } = new MvxObservableCollection<ContactModel>();

        public ContactsTabViewModel(
            ICommandFactory commandFactory,
            ILogger<BaseViewModel> logger,
            IMvxNavigationService navigationService,
            IContactFacade contactFacade,
            IMvxMessenger messenger
        ) : base(commandFactory, logger)
        {
            this.commandFactory = commandFactory;
            this.logger = logger;
            this.navigationService = navigationService;

            NavigateToSettingsCommand = commandFactory.CreateAsyncCommand((_) => NavigateToSettingsPageAsync(), () => true);
            this.contactFacade = contactFacade;
            this.messenger = messenger;
        }

        public override Task Initialize()
        {
            commandFactory.CreateObserverFor(LoadContactsAsync);
            connectedSubscription = messenger.SubscribeOnMainThread<ConnectOfferAcknowledgedMessage>(
                (notification) =>
                {
                    logger.LogDebug("Contact view model got a new contact offer acknowledged message.");
                    Contacts.Add(notification.Contact);
                });

            connectAckSubscription = messenger.SubscribeOnMainThread<MyAcknowledgedConnectOfferMessage>((notification) =>
            {
                logger.LogDebug("Contact view model got a new MY contact offer got acknowledged message.");
                Contacts.Add(notification.Contact);
            });
            return base.Initialize();
        }

        private readonly ICommandFactory commandFactory;
        private readonly ILogger<BaseViewModel> logger;
        private readonly IMvxNavigationService navigationService;
        private readonly IContactFacade contactFacade;
        private readonly IMvxMessenger messenger;

        /// <summary>
        /// Save subscription reference
        /// </summary>
        private MvxSubscriptionToken? connectedSubscription;

        private MvxSubscriptionToken? connectAckSubscription;

        private async Task NavigateToSettingsPageAsync()
        {
            await navigationService.Navigate<SettingsViewModel>();
        }

        private async Task LoadContactsAsync()
        {
            IEnumerable<ContactModel>? contacts = await contactFacade.GetExistingContactsAsync();
            if (!contacts.Any())
            {
                logger.LogWarning("No contacts are available to display.");
                return;
            }

            Contacts.AddRange(contacts);
        }
    }
}
