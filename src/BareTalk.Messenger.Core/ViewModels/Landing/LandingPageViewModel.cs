using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Core.Commands.Factory;
using Microsoft.Extensions.Logging;
using MvvmCross.Commands;
using MvvmCross.Navigation;

namespace BareTalk.Messenger.Core.ViewModels.Landing
{
    public sealed class LandingPageViewModel : BaseViewModel
    {
        private readonly IMvxNavigationService navigationService;

        public LandingPageViewModel(ICommandFactory commandFactory, ILogger<BaseViewModel> logger, IMvxNavigationService navigationService) : base(commandFactory, logger)
        {
            this.navigationService = navigationService;
            ShowInitialViewModelsCommand = new MvxAsyncCommand(ShowInitialViewModelsAsync);
        }

        public IMvxAsyncCommand ShowInitialViewModelsCommand { get; private set; }

        private async Task ShowInitialViewModelsAsync()
        {
            var tasks = new List<Task>
            {
                navigationService.Navigate<ConversationsTabViewModel>(),
                navigationService.Navigate<ContactsTabViewModel>()
            };

            await Task.WhenAll(tasks);
        }
    }
}
