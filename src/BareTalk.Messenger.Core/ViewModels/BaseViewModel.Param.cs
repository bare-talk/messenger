using BareTalk.Messenger.Core.Commands.Factory;
using Microsoft.Extensions.Logging;
using MvvmCross.ViewModels;

namespace BareTalk.Messenger.Core.ViewModels
{
    public abstract class BaseViewModel<TParameter> : BaseViewModel, IMvxViewModel<TParameter>
        where TParameter : notnull
    {
        protected BaseViewModel(ICommandFactory commandFactory, ILogger<BaseViewModel> logger) : base(commandFactory, logger)
        {
        }

        public abstract void Prepare(TParameter parameter);
    }
}
