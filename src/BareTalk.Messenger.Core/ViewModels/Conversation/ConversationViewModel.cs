using System.Collections.Generic;
using System.Threading.Tasks;
using BareTalk.Messenger.Core.Commands.Factory;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;
using BareTalk.Messenger.Messenger.Messages;
using Microsoft.Extensions.Logging;
using MvvmCross.Commands;
using MvvmCross.Plugin.Messenger;
using MvvmCross.ViewModels;
using Plugin.ValidationRules;

namespace BareTalk.Messenger.Core.ViewModels.Conversation
{
    public class ConversationViewModel : BaseViewModel<FriendlyPrivateConversationModel>
    {
        private readonly IMessageFacade messageFacade;
        private readonly IMvxMessenger messenger;
        private readonly ILogger<BaseViewModel> logger;

        #region NavigationData
        public FriendlyPrivateConversationModel Conversation { get; private set; } = null!;
        #endregion NavigationData

        #region Data
        public MvxObservableCollection<ConversationMessageModel> Messages { get; } = new MvxObservableCollection<ConversationMessageModel>();
        public Validatable<string> Message { get; }
        #endregion Data

        #region Commands
        public IMvxCommand SendMessageCommand { get; }
        #endregion Commands

        #region Notifiers
        public MvxNotifyTask? MoreMessagesAvailable { get; set; }
        public MvxNotifyTask? MessageSent { get; set; }
        public MvxNotifyTask? LatestMessagesLoaded { get; set; }
        #endregion Notifiers

        private MvxSubscriptionToken? chatMessageSubscription;

        public ConversationViewModel(
            ICommandFactory commandFactory,
            ILogger<BaseViewModel> logger,
            IMessageFacade messageFacade,
            IMvxMessenger messenger
        ) : base(commandFactory, logger)
        {
            Message = new Validatable<string>();

            this.logger = logger;
            this.messageFacade = messageFacade;

            SendMessageCommand = commandFactory.Create(() => MessageSent = commandFactory.CreateObserverFor(() => SendMessageAsync()), () => Message.IsValid);
            this.messenger = messenger;
        }

        public override void Prepare(FriendlyPrivateConversationModel conversation)
        {
            logger.LogTrace("Conversation page prepared with conversation for user {@User}", conversation.Contact.DisplayName);
            Conversation = conversation;
        }

        public override async Task Initialize()
        {
            LatestMessagesLoaded = CommandFactory.CreateObserverFor(LoadLatestMessagesAsync);
            chatMessageSubscription = messenger.SubscribeOnMainThread<ChatMessage>((notification) =>
            {
                if (notification.Message.ConversationIdentifier.Value != Conversation.Identifier.Value)
                {
                    logger.LogWarning("The chat message was not intended for this conversation.");
                    return;
                }

                Messages.Add(notification.Message);
                logger.LogDebug("Added new message to conversation for contact {@Contact}", Conversation.Contact.Username.Value);
            });
            await base.Initialize();
        }

        private async Task LoadLatestMessagesAsync()
        {
            logger.LogTrace("Initializing conversation messages");
            IEnumerable<ConversationMessageModel> latestMessages = await messageFacade.GetLatestMessagesAsync(Conversation.Identifier);
            Messages.AddRange(latestMessages);
            logger.LogTrace("Successfully initialized conversation messages");
        }

        private async Task SendMessageAsync()
        {
            if (!Message.Validate())
            {
                logger.LogWarning("Preventing message sending as the message is invalid.");
                return;
            }

            ConversationMessageModel? sent = await messageFacade.EncryptAndSendAsync(new NewMessageModel(new Message(Message.Value), Conversation.Identifier));

            if (sent == null)
            {
                logger.LogError("Failed to send chat message.");
                return;
            }

            logger.LogDebug("Successfully sent a chat message");
            Messages.Add(sent);
        }

        public override void ViewDestroy(bool viewFinishing = true)
        {
            chatMessageSubscription?.Dispose();
            base.ViewDestroy(viewFinishing);
        }
    }
}
