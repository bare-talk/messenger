using System.Threading.Tasks;
using BareTalk.Messenger.Core.Commands.Factory;
using BareTalk.Messenger.Core.ViewModels.Login;
using BareTalk.Messenger.Domain.Contracts.Application;
using Microsoft.Extensions.Logging;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace BareTalk.Messenger.Core.ViewModels.Settings
{
    public class SettingsViewModel : BaseViewModel
    {
        #region Commands
        public IMvxCommand LogOutCommand { get; set; }
        #endregion Commands

        #region Notifiers
        public MvxNotifyTask? LogOutNotifier { get; private set; }
        #endregion Notifiers

        #region Dependencies
        private readonly IAuthenticationFacade authenticationFacade;
        private readonly IMvxNavigationService navigationService;
        #endregion Dependencies

        public SettingsViewModel(
            ICommandFactory commandFactory,
            ILogger<BaseViewModel> logger,
            IAuthenticationFacade authenticationFacade,
            IMvxNavigationService navigationService
        ) : base(commandFactory, logger)
        {
            LogOutCommand = commandFactory.Create(
                () => LogOutNotifier = commandFactory.CreateObserverFor(LogOutAsync),
                () => true
             );
            this.authenticationFacade = authenticationFacade;
            this.navigationService = navigationService;
        }

        private async Task LogOutAsync()
        {
            var loggedOut = await authenticationFacade.LogOutAsync();
            if (loggedOut)
            {
                await navigationService.Navigate<LoginViewModel>();
            }
        }
    }
}
