using BareTalk.Messenger.Core.Commands.Factory;
using Microsoft.Extensions.Logging;
using MvvmCross.ViewModels;

namespace BareTalk.Messenger.Core.ViewModels
{
    public abstract class BaseViewModel : MvxViewModel
    {
        protected readonly ICommandFactory CommandFactory;
        protected readonly ILogger<BaseViewModel> Logger;

        protected BaseViewModel(ICommandFactory commandFactory, ILogger<BaseViewModel> logger)
        {
            CommandFactory = commandFactory;
            Logger = logger;
        }
    }
}
