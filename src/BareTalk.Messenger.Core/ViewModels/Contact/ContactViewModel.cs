using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BareTalk.Messenger.Core.Commands.Factory;
using BareTalk.Messenger.Domain.Constants;
using BareTalk.Messenger.Domain.Contracts.Authentication;
using BareTalk.Messenger.Domain.Contracts.Pki;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;
using BareTalk.Messenger.Validation.Rules;
using Microsoft.Extensions.Logging;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using Plugin.ValidationRules;

namespace BareTalk.Messenger.Core.ViewModels.Contact
{
    public class ContactViewModel : BaseViewModelResult<NewContactModel>
    {
        #region Commands
        public IMvxCommand<AvailableContactModel> AddContactCommand { get; }
        public IMvxCommand SearchForContactCommand { get; }

        #endregion Commands

        #region Data
        public MvxObservableCollection<AvailableContactModel> LoadedAvailableContacts { get; set; } = new MvxObservableCollection<AvailableContactModel>();

        public Validatable<string> Query { get; }
        #endregion Data

        private readonly ILogger<BaseViewModel> logger;
        private readonly IMvxNavigationService navigationService;
        private readonly IClaimPublicKeys claimer;
        private readonly IAuthenticationService authenticationService;

        #region Notifiers

        private MvxNotifyTask? availableContactsLoaded;

        public MvxNotifyTask? AvailableContactsLoaded
        {
            get => availableContactsLoaded;
            set
            {
                availableContactsLoaded = value;
                RaisePropertyChanged(nameof(AvailableContactsLoaded));
            }
        }

        private MvxNotifyTask? contactAdded;

        public MvxNotifyTask? ContactAdded
        {
            get => contactAdded; set
            {
                contactAdded = value;
                RaisePropertyChanged(nameof(ContactAdded));
            }
        }

        #endregion Notifiers

        public ContactViewModel(
            ICommandFactory commandFactory,
            ILogger<BaseViewModel> logger,
            IMvxNavigationService navigationService,
            IClaimPublicKeys claimer,
            IAuthenticationService authenticationService
        ) : base(commandFactory, logger)
        {
            Query = new Validatable<string>();
            AddValidationRules();

            AddContactCommand = commandFactory.Create<AvailableContactModel>((contact) => ContactAdded = commandFactory.CreateObserverFor(() => AddContactAsync(contact)), (_) => true);

            SearchForContactCommand = commandFactory.Create(() => AvailableContactsLoaded = commandFactory.CreateObserverFor(() => QueryForContactsAsync()), () => Query.IsValid);

            this.logger = logger;
            this.navigationService = navigationService;
            this.claimer = claimer;
            this.authenticationService = authenticationService;
        }

        private void AddValidationRules()
        {
            // username
            Query.Validations.Add(new StringLengthRule(ValidationConstants.MinUsernameLength, ValidationConstants.MaxUsernameLength, $"The username needs to be between {ValidationConstants.MinUsernameLength} and {ValidationConstants.MaxUsernameLength} characters long."));
            Query.Validations.Add(new StringRegexPatternRule(ValidationRegexes.UsernameRegex, "The username accepts only a-z, 0-9, and _ characters."));
        }

        private async Task AddContactAsync(AvailableContactModel contact)
        {
            KeyPairModel? claimedKey = await claimer.ClaimKeyAsync(contact.Owner);
            if (claimedKey != null)
            {
                await navigationService.Close(this, new NewContactModel(contact.Owner, claimedKey.Identifier, contact.DisplayName, claimedKey.PublicKey));
            }
        }

        private async Task QueryForContactsAsync()
        {
            if(!Query.Validate())
            {
                return;
            }

            logger.LogInformation("Attempting to query keys for {@Query}", Query.Value);
            LoadedAvailableContacts.Clear();
            UserModel? user = await authenticationService.GetAuthenticatedUser();
            if (user == null)
            {
                logger.LogError("The currently authenticated user was null when querying for users.");
                return;
            }

            IEnumerable<AvailableContactModel> results = await claimer.QueryAvailableKeysFor(user.Username, new Username(Query.Value));
            logger.LogInformation("Got {@Number} keys for {@Query}", results.Count(), Query.Value);
            LoadedAvailableContacts.AddRange(results);
        }
    }
}
