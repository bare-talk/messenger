using BareTalk.Messenger.Core.Commands.Factory;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Core.ViewModels.Main
{
    public class MainViewModel : BaseViewModel
    {
        public MainViewModel(ICommandFactory commandFactory, ILogger<BaseViewModel> logger) : base(commandFactory, logger)
        {
        }
    }
}
