using System.Threading.Tasks;
using BareTalk.Messenger.Core.Commands.Factory;
using BareTalk.Messenger.Core.ViewModels.Landing;
using BareTalk.Messenger.Core.ViewModels.Register;
using BareTalk.Messenger.Domain.Constants;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Validation.Rules;
using Microsoft.Extensions.Logging;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using Plugin.ValidationRules;

namespace BareTalk.Messenger.Core.ViewModels.Login
{
    public class LoginViewModel : BaseViewModel
    {
        #region Commands
        public IMvxCommand LoginCommand { get; set; }
        public IMvxAsyncCommand GoToRegistrationPage { get; set; }
        #endregion Commands

        #region Notifiers
        private MvxNotifyTask loggingIn = MvxNotifyTask.Create(Task.CompletedTask);

        public MvxNotifyTask LoggingIn
        {
            get => loggingIn;
            set
            {
                loggingIn = value;
                RaisePropertyChanged(nameof(LoggingIn));
            }
        }

        #endregion Notifiers

        #region Validation
        public Validatable<string> Username { get; set; }
        public Validatable<string> Password { get; set; }
        public ValidationUnit FormValidationStatus { get; private set; }
        #endregion Validation

        public LoginViewModel(
            ICommandFactory commandFactory,
            ILogger<BaseViewModel> logger,
            IAuthenticationFacade authenticationFacade,
            IMvxNavigationService navigationService
        ) : base(commandFactory, logger)
        {
            Username = new Validatable<string>();
            Password = new Validatable<string>();
            AddValidationRules();
            FormValidationStatus = new ValidationUnit(Username, Password)
            {
                IsValid = false
            };

            this.authenticationFacade = authenticationFacade;
            this.navigationService = navigationService;

            LoginCommand = commandFactory.Create(() => LoggingIn = commandFactory.CreateObserverFor(() => SignInAsync()), () => true);
            GoToRegistrationPage = commandFactory.CreateAsyncCommand((_) => NavigateToRegistrationPageAsync(), () => true);
        }

        private readonly IAuthenticationFacade authenticationFacade;
        private readonly IMvxNavigationService navigationService;

        private void AddValidationRules()
        {
            // username
            Username.Validations.Add(new StringLengthRule(ValidationConstants.MinUsernameLength, ValidationConstants.MaxUsernameLength, $"The username needs to be between {ValidationConstants.MinUsernameLength} and {ValidationConstants.MaxUsernameLength} characters long."));
            Username.Validations.Add(new StringRegexPatternRule(ValidationRegexes.UsernameRegex, "The username accepts only a-z, 0-9, and _ characters."));

            // password
            Password.Validations.Add(new StringLengthRule(ValidationConstants.MinPasswordLength, ValidationConstants.MaxPasswordLength, $"The password needs to be between {ValidationConstants.MinPasswordLength} and {ValidationConstants.MaxPasswordLength} characters long."));
            Password.Validations.Add(new StringRegexPatternRule(ValidationRegexes.PasswordRegex, "The password does not contain valid characters. Allowed characters are a-z, 0-9, !@#$%^&*()_+=|[]`~"));
        }

        private async Task SignInAsync()
        {
            var successfullyAuthenticated = await authenticationFacade.AuthenticateAsync(Username.Value, Password.Value);
            if (successfullyAuthenticated)
            {
                await navigationService.Navigate<LandingPageViewModel>();
            }
        }

        private async Task NavigateToRegistrationPageAsync()
        {
            await navigationService.Navigate<RegisterViewModel>();
        }
    }
}
