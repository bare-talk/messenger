using System.Threading.Tasks;
using BareTalk.Messenger.Core.Commands.Factory;
using Microsoft.Extensions.Logging;
using MvvmCross.ViewModels;

namespace BareTalk.Messenger.Core.ViewModels
{
    public abstract class BaseViewModelResult<TResult> : BaseViewModel, IMvxViewModelResult<TResult>
        where TResult : notnull
    {
        protected BaseViewModelResult(ICommandFactory commandFactory, ILogger<BaseViewModel> logger) : base(commandFactory, logger)
        {
        }

        public TaskCompletionSource<object> CloseCompletionSource { get; set; }

        public override void ViewDestroy(bool viewFinishing = true)
        {
            if (viewFinishing && CloseCompletionSource?.Task.IsCompleted == false && !CloseCompletionSource.Task.IsFaulted)
                CloseCompletionSource?.TrySetCanceled();

            base.ViewDestroy(viewFinishing);
        }
    }
}
