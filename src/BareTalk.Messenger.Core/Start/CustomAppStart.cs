using System;
using System.Threading.Tasks;
using BareTalk.Messenger.Core.ViewModels.Landing;
using BareTalk.Messenger.Core.ViewModels.Login;
using BareTalk.Messenger.Domain.Contracts.Authentication;
using BareTalk.Messenger.Domain.Contracts.Rtc;
using BareTalk.Messenger.Domain.Entities;
using Microsoft.Extensions.Logging;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace BareTalk.Messenger.Core.Start
{
    public class CustomAppStart : MvxAppStart
    {
        private readonly IAuthenticationService authenticationService;
        private readonly IRtcClient rtcClient;
        private readonly ILogger<CustomAppStart> logger;

        public CustomAppStart(
            IMvxApplication application,
            IMvxNavigationService navigationService,
            ILogger<CustomAppStart> logger,
            IAuthenticationService authenticationService,
            IRtcClient rtcClient
        ) : base(application, navigationService)
        {
            this.logger = logger;
            this.authenticationService = authenticationService;
            this.rtcClient = rtcClient;
        }

        protected override Task NavigateToFirstViewModel(object hint = null)
        {
            try
            {
                // avoid async/await, as per documentation
                //https://www.mvvmcross.com/documentation/advanced/customizing-appstart?scroll=464
                var taskCompletionSource = new TaskCompletionSource<bool>();
                Task.Run(async () =>
                {
                    var result = await authenticationService.AuthenticateWithExistingCredentialsAsync();
                    taskCompletionSource.SetResult(result);
                });

                var isAuthenticated = taskCompletionSource.Task.Result;

                if (isAuthenticated)
                {
                    Task.Run(async () =>
                    {
                        UserModel? user = await authenticationService.GetAuthenticatedUser();
                        await rtcClient.StartAsync(user.Username);
                    });
                    NavigationService.Navigate<LandingPageViewModel>().GetAwaiter().GetResult();
                }
                else
                {
                    NavigationService.Navigate<LoginViewModel>().GetAwaiter().GetResult();
                }
            }
            catch (Exception exception)
            {
                logger.LogCritical("Failed to perform the initial navigation. Error: {@Error}", exception.Message);
            }

            return Task.CompletedTask;
        }
    }
}
