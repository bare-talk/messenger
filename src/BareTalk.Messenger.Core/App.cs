using BareTalk.Messenger.Bootstrapper;
using BareTalk.Messenger.Core.Commands.Factory;
using BareTalk.Messenger.Core.Start;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.ViewModels;

namespace BareTalk.Messenger.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            RegisterDependencies();
            RegisterCustomAppStart<CustomAppStart>();
        }

        private void RegisterDependencies()
        {
            ILogger logger = Mvx.IoCProvider.Resolve<ILogger<App>>();
            Mvx.IoCProvider.RegisterType<ICommandFactory, CommandFactory>();
            Mvx.IoCProvider.BootstrapApplicationDependencies(logger);
        }
    }
}
