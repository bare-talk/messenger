using System;
using BareTalk.Messenger.Domain.Contracts.Encryption;
using Chaos.NaCl;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Encryption.Sign
{
    public sealed class Ed25519Signer : ISignMessages
    {
        private readonly ILogger<Ed25519Signer> logger;

        public Ed25519Signer(ILogger<Ed25519Signer> logger)
        {
            this.logger = logger;
        }

        public byte[] Sign(byte[] message, byte[] expandedPrivateKey)
        {
            ThrowOnInvalidPrivateKeySize(expandedPrivateKey);

            logger.LogTrace("Attempting to sign {@Bytes} bytes", message.Length);

            try
            {
                return Ed25519.Sign(message, expandedPrivateKey);
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to sign the provided message using Ed25519. Error: {@Error}", exception.Message);
            }

            return new byte[0];
        }

        public bool Verify(byte[] signature, byte[] message, byte[] publicKey)
        {
            ThrowOnInvalidPublicKeySize(publicKey);

            logger.LogTrace("Attempting to verify {@Bytes} bytes", message.Length);

            try
            {
                return Ed25519.Verify(signature, message, publicKey);
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to verify the provided message using Ed25519. Error: {@Error}", exception.Message);
            }

            return false;
        }

        private void ThrowOnInvalidPrivateKeySize(byte[] expandedPrivateKey)
        {
            if (expandedPrivateKey.Length != 64)
            {
                logger.LogError("Invalid private key provided. The private key must be {@Bytes} bytes long", 64);

                throw new ArgumentException("The expanded private key must be 64 bytes long.", nameof(expandedPrivateKey));
            }
        }

        private void ThrowOnInvalidPublicKeySize(byte[] publicKey)
        {
            if (publicKey.Length != 32)
            {
                logger.LogError("Invalid public key provided. The public key must be {@Bytes} bytes long", 32);
                throw new ArgumentException("The expanded public key must be 32 bytes long.", nameof(publicKey));
            }
        }
    }
}
