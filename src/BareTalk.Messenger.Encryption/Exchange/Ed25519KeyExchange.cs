using System;
using BareTalk.Messenger.Domain.Contracts.Encryption;
using Chaos.NaCl;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Encryption.Exchange
{
    public class Ed25519KeyExchange : IExchangeKeys
    {
        private readonly ILogger<Ed25519KeyExchange> logger;

        public Ed25519KeyExchange(ILogger<Ed25519KeyExchange> logger)
        {
            this.logger = logger;
        }

        public byte[] KeyExchange(byte[] publicKey, byte[] privateKey)
        {
            try
            {
#pragma warning disable CS0618 // Type or member is obsolete
                return Ed25519.KeyExchange(publicKey, privateKey);
#pragma warning restore CS0618 // Type or member is obsolete
            }
            catch (Exception exception)
            {
                logger.LogError("Key exchange failed. Error: {@Error}", exception.Message);
            }

            return new byte[0];
        }
    }
}
