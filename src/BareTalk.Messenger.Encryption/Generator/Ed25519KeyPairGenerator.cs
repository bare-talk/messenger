using System;
using BareTalk.Messenger.Domain.Contracts.Encoder;
using BareTalk.Messenger.Domain.Contracts.Encryption;
using BareTalk.Messenger.Domain.ValueObjects;
using Chaos.NaCl;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Encryption.Generator
{
    public class Ed25519KeyPairGenerator : IGenerateKeys
    {
        private readonly IDecode base64Decoder;
        private readonly IGenerateSalt saltGenerator;
        private readonly ILogger<Ed25519KeyPairGenerator> logger;

        public Ed25519KeyPairGenerator(
            IDecode base64Decoder,
            IGenerateSalt saltGenerator,
            ILogger<Ed25519KeyPairGenerator> logger
        )
        {
            this.base64Decoder = base64Decoder;
            this.saltGenerator = saltGenerator;
            this.logger = logger;
        }

        public (PrivateKey?, PublicKey?) Generate()
        {
            var seed = saltGenerator.GenerateSecureSalt();
            if (seed.Length == 0)
            {
                logger.LogError("Aborting key pair generation due to missing seed.");
                return (null, null);
            }

            var generationSucceeded = GenerateKeys(seed, out var expandedPrivateKey, out var publicKey);

            if (!generationSucceeded)
            {
                logger.LogError("Aborting key pair generation due to an error.");
                return (null, null);
            }

            var decodedPublicKey = base64Decoder.Decode(publicKey);
            var decodedPrivateKey = base64Decoder.Decode(expandedPrivateKey);

            if (decodedPrivateKey.Length == 0 || decodedPublicKey.Length == 0)
            {
                logger.LogError("Aborting key pair generation due base 64 decoding errors.");
                return (null, null);
            }

            return (new PrivateKey(decodedPrivateKey), new PublicKey(decodedPublicKey));
        }

        private bool GenerateKeys(byte[] seed, out byte[] expandedPrivateKey, out byte[] publicKey)
        {
            publicKey = new byte[32];
            expandedPrivateKey = new byte[64];

            try
            {
                Ed25519.KeyPairFromSeed(publicKey, expandedPrivateKey, seed);
                return true;
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to generate the key pair using Ed25519. Error: {@Error}", exception.Message);
            }

            return false;
        }
    }
}
