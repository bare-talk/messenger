using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using BareTalk.Messenger.Domain.Contracts.Encoder;
using BareTalk.Messenger.Domain.Contracts.Encryption;

namespace BareTalk.Messenger.Encryption.Decryptor
{
    public class AesDecryptor : IDecrypt
    {
        private readonly IDecode utf8Decoder;

        public AesDecryptor(IDecode utf8Decoder)
        {
            this.utf8Decoder = utf8Decoder;
        }

        public string Decrypt(byte[] encryptedMessageBytes, byte[] sharedKey)
        {
            var utf8 = new UTF8Encoding(false);
            var initializationVector = ExtractIVFromCipherText(encryptedMessageBytes);
            ICryptoTransform decryptor = BuildDecryptor(sharedKey, initializationVector);

            using var plaintextStream = new MemoryStream();
            using (var decryptionStream = new CryptoStream(plaintextStream, decryptor, CryptoStreamMode.Write))
            {
                using var binaryWriter = new BinaryWriter(decryptionStream, utf8);

                // skip the first 16 bytes when deciphering, as it contains the plain text IV
                binaryWriter.Write(
                    encryptedMessageBytes,
                    initializationVector.Length,
                    encryptedMessageBytes.Length - initializationVector.Length
                );
            }

            return utf8Decoder.Decode(plaintextStream.ToArray());
        }

        private ICryptoTransform BuildDecryptor(byte[] sharedKey, byte[] initializationVector)
        {
            using var aesAlg = new AesCryptoServiceProvider()
            {
                Key = sharedKey,
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                IV = initializationVector,
            };

            // Create a decryptor to perform the stream transform.
            return aesAlg.CreateDecryptor(aesAlg.Key, initializationVector);
        }

        private byte[] ExtractIVFromCipherText(byte[] cipherText)
        {
            var initializationVector = new byte[16];
            Array.Copy(cipherText, 0, initializationVector, 0, initializationVector.Length);
            return initializationVector;
        }
    }
}
