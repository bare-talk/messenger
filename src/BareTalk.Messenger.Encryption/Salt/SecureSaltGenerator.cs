using System;
using System.Security.Cryptography;
using BareTalk.Messenger.Domain.Contracts.Encryption;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Encryption.Salt
{
    public class SecureSaltGenerator : IGenerateSalt
    {
        private readonly ILogger<SecureSaltGenerator> logger;

        public SecureSaltGenerator(ILogger<SecureSaltGenerator> logger)
        {
            this.logger = logger;
        }

        public byte[] GenerateSecureSalt(int numberOfBytes = 32)
        {
            logger.LogTrace("Attempting to generate {@Bytes} number of secure random bytes.", numberOfBytes);
            try
            {
                var result = new byte[numberOfBytes];
                RandomNumberGenerator.Create().GetBytes(result);
                logger.LogDebug("Successfully generate {@Bytes} secure random bytes", numberOfBytes);
                return result;
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to generate a random array of size {@size}. Error: {@Error}", numberOfBytes, exception.Message);
            }

            return new byte[0];
        }
    }
}
