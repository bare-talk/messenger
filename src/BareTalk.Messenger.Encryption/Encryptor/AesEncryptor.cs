using System.IO;
using System.Security.Cryptography;
using System.Text;
using BareTalk.Messenger.Domain.Contracts.Encryption;

namespace BareTalk.Messenger.Encryption.Encryptor
{
    public sealed class AesEncryptor : IEncrypt
    {
        public byte[] Encrypt(string plainTextMessage, byte[] sharedKey)
        {
            var utf8 = new UTF8Encoding(false);
            using var aesAlg = new AesCryptoServiceProvider()
            {
                Key = sharedKey,
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };

            aesAlg.GenerateIV();
            var initializationVector = aesAlg.IV;
            byte[] encrypted;

            using (var cipherText = new MemoryStream())
            {
                // IV should not be encrypted
                cipherText.Write(initializationVector);

                using ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, initializationVector);
                using (var encryptionStream = new CryptoStream(cipherText, encryptor, CryptoStreamMode.Write))

                //Write all data to the encrypted stream using a writer
                using (var streamWriter = new StreamWriter(encryptionStream, utf8))
                {
                    streamWriter.Write(plainTextMessage);
                }

                encrypted = cipherText.ToArray();
            }

            return encrypted;
        }
    }
}
