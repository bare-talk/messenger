using System.Security.Cryptography;
using BareTalk.Messenger.Domain.Contracts.Encryption;

namespace BareTalk.Messenger.Encryption.Derivation
{
    public class Pbkdf2Wrapper : IDeriveBytes
    {
        private const int NumberOfIterations = 5000;
        private const int NumberOfDerivedBytes = 32;

        public byte[] GetBytes(string password, byte[] salt)
        {
            using var pbkd = new Rfc2898DeriveBytes(password, salt, NumberOfIterations);
            return pbkd.GetBytes(NumberOfDerivedBytes);
        }
    }
}
