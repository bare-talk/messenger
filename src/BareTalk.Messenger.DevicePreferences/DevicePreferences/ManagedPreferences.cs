using Microsoft.Extensions.Logging;
using Xamarin.Essentials;

namespace BareTalk.Messenger.DevicePreferences.DevicePreferences
{
    public sealed class ManagedPreferences : IDevicePreferences
    {
        private readonly ILogger<ManagedPreferences> logger;

        public ManagedPreferences(ILogger<ManagedPreferences> logger)
        {
            this.logger = logger;
        }

        public bool Get(string key, bool defaultValue = false)
        {
            logger.LogTrace("Retrieving {@Key} from preferences API.");
            return Preferences.Get(key, defaultValue);
        }

        public void Set(string key, bool value)
        {
            logger.LogTrace("Setting {@Key} in preferences API.");
            Preferences.Set(key, value);
        }
    }
}
