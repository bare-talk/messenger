namespace BareTalk.Messenger.DevicePreferences.DevicePreferences
{
    public interface IDevicePreferences
    {
        bool Get(string key, bool defaultValue = false);

        void Set(string key, bool value);
    }
}
