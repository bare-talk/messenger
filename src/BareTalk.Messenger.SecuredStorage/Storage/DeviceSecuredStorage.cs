using System;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.SecuredStorage;
using Microsoft.Extensions.Logging;
using Xamarin.Essentials;

namespace BareTalk.Messenger.SecuredStorage.Storage
{
    public sealed class DeviceSecuredStorage : ISecuredStorage
    {
        private readonly ILogger<DeviceSecuredStorage> logger;

        public DeviceSecuredStorage(ILogger<DeviceSecuredStorage> logger)
        {
            this.logger = logger;
        }

        public async Task SetAsync(string key, string value)
        {
            ThrowOnEmptyKey(key);

            logger.LogDebug("Attempting to set {@Key} to {@Value}, in secure storage.", key, value);

            try
            {
                await SecureStorage.SetAsync(key, value);
                logger.LogDebug("Successfully set {@Key} to {@Value}, in secure storage.", key, value);
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to set {@key} to {@Value}. Error: {@Error}", key, value, exception.Message);
            }
        }

        public async Task<string> GetAsync(string key)
        {
            ThrowOnEmptyKey(key);

            logger.LogDebug("Attempting to get {@Key} from secure storage.", key);
            try
            {
                var result = await SecureStorage.GetAsync(key);
                logger.LogDebug("Successfully retrieved {@Key} from secure storage.", key);
                return result;
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to get {@key} from secure storage. Error: {@Error}", key, exception.Message);
            }

            return string.Empty;
        }

        public string Get(string key)
        {
            return GetAsync(key).GetAwaiter().GetResult();
        }

        public bool Remove(string key)
        {
            ThrowOnEmptyKey(key);

            try
            {
                logger.LogTrace("Attempting to remove key {@Key} from secured storage", key);
                return SecureStorage.Remove(key);
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to remove {@Key} from secured storage. Error: {@Error}", key, exception.Message);
            }

            return false;
        }

        private void ThrowOnEmptyKey(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                logger.LogError("Empty key provided to secured storage.");
                throw new ArgumentException($"Key {key} was null or empty", nameof(key));
            }
        }
    }
}
