using System;
using Android.App;
using Android.OS;
using BareTalk.Messenger.Core.ViewModels.Main;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Domain.Contracts.Rtc;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Messenger.Messages;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Forms.Platforms.Android.Views;
using MvvmCross.Plugin.Messenger;

namespace BareTalk.Messenger.Droid
{
    [Activity(Theme = "@style/AppTheme", MainLauncher = false)]
    public class MainActivity : MvxFormsAppCompatActivity<MainViewModel>
    {
        private IDisposable messageReceivedSubscription;
        private IDisposable contactOfferReceivedSubscription;
        private IDisposable contactOfferAcknowledgedSubscription;

        private readonly ILogger<MainActivity> logger = Mvx.IoCProvider.Resolve<ILogger<MainActivity>>();
        private readonly IRtcClient rtcClient = Mvx.IoCProvider.Resolve<IRtcClient>();
        private readonly IMessageFacade messageFacade = Mvx.IoCProvider.Resolve<IMessageFacade>();
        private readonly IContactFacade contactConnectionFacade = Mvx.IoCProvider.Resolve<IContactFacade>();
        private readonly IMvxMessenger messenger = Mvx.IoCProvider.Resolve<IMvxMessenger>();

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            Xamarin.Forms.FormsMaterial.Init(this, bundle);
            Xamarin.Essentials.Platform.Init(this, bundle);
            base.OnCreate(bundle);
        }

        protected override void OnStart()
        {
            messageReceivedSubscription = rtcClient.NewMessageReceived.Subscribe(async (encryptedMessage) =>
            {
                logger.LogDebug("Received a new encrypted chat message.");
                ConversationMessageModel messageModel = await messageFacade.DecryptAndSaveAsync(encryptedMessage);
                if (messageModel != null)
                {
                    messenger.Publish(new ChatMessage(this, messageModel));
                }
            });

            contactOfferReceivedSubscription = rtcClient.ContactOfferReceived.Subscribe(async (offer) =>
            {
                logger.LogDebug("Received a new connection offer.");
                ContactModel conversation = await contactConnectionFacade.AcknowledgeIncomingConnectionOfferAsync(offer);
                if (conversation != null)
                {
                    messenger.Publish(new ConnectOfferAcknowledgedMessage(this, conversation));
                }
            });

            contactOfferAcknowledgedSubscription = rtcClient.ContactOfferAcknowledged.Subscribe(async (acknowledgedOffer) =>
            {
                logger.LogDebug("Received connect acknowledgment.");
                ContactModel conversation = await contactConnectionFacade.CompleteConnectAsync(acknowledgedOffer);
                if (conversation != null)
                {
                    messenger.Publish(new MyAcknowledgedConnectOfferMessage(this, conversation));
                }
            });

            base.OnStart();
        }

        protected override void OnStop()
        {
            messageReceivedSubscription?.Dispose();
            contactOfferReceivedSubscription?.Dispose();
            contactOfferAcknowledgedSubscription?.Dispose();
            rtcClient.StopAsync().GetAwaiter().GetResult();
            rtcClient.DisposeAsync().GetAwaiter().GetResult();
            base.OnStop();
        }
    }
}
