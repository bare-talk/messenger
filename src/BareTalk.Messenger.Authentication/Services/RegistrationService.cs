using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.Authentication;
using BareTalk.Messenger.Domain.Contracts.Encoder;
using BareTalk.Messenger.Domain.Contracts.Encryption;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.Repository;
using BareTalk.Messenger.Domain.ValueObjects;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Authentication.Services
{
    public sealed class RegistrationService : IRegistrationService
    {
        private readonly IUserRepository userRepository;
        private readonly IGenerateSalt saltGenerator;
        private readonly IDeriveBytes pbkdf2;
        private readonly IDecode base64Decoder;
        private readonly ILogger<RegistrationService> logger;

        public RegistrationService(
            IUserRepository userRepository,
            IGenerateSalt saltGenerator,
            IDeriveBytes pbkdf2,
            IDecode base64Decoder,
            ILogger<RegistrationService> logger
        )
        {
            this.userRepository = userRepository;
            this.saltGenerator = saltGenerator;
            this.pbkdf2 = pbkdf2;
            this.base64Decoder = base64Decoder;
            this.logger = logger;
        }

        public async Task<UserModel?> ExistsAsync(Username username)
        {
            return await userRepository.FindByUsernameAsync(username.Value);
        }

        public async Task<UserModel?> RegisterAsync(
            Username username,
            DisplayName displayName,
            Password passwordValueObject
        )
        {
            var saltBytes = saltGenerator.GenerateSecureSalt();
            var passwordBytes = pbkdf2.GetBytes(passwordValueObject.Value, saltBytes);

            if (await userRepository.FindByUsernameAsync(username.Value) != null)
            {
                logger.LogError("User {@User} already exists.", username.Value);
                return null;
            }

            return await userRepository.Create(
                username.Value,
                displayName.Value,
                base64Decoder.Decode(saltBytes),
                base64Decoder.Decode(passwordBytes)
            );
        }
    }
}
