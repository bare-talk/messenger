using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Constants;
using BareTalk.Messenger.Domain.Contracts.Authentication;
using BareTalk.Messenger.Domain.Contracts.DataAccess;
using BareTalk.Messenger.Domain.Contracts.SecuredStorage;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.Repository;
using BareTalk.Messenger.Domain.ValueObjects;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Authentication.Services
{
    public sealed class AuthenticationService : IAuthenticationService
    {
        private readonly IUserRepository userRepository;
        private readonly ILogger<AuthenticationService> logger;
        private readonly ISecuredStorage securedStorage;
        private readonly IDatabaseConnectionBuilder databaseConnectionBuilder;

        public AuthenticationService(
            IUserRepository userRepository,
            ILogger<AuthenticationService> logger,
            ISecuredStorage securedStorage,
            IDatabaseConnectionBuilder databaseConnectionBuilder
        )
        {
            this.userRepository = userRepository;
            this.logger = logger;
            this.securedStorage = securedStorage;
            this.databaseConnectionBuilder = databaseConnectionBuilder;
        }

        public async Task<UserModel?> AuthenticateAsync(Username username, Password password)
        {
            await securedStorage.SetAsync(WellKnownStorageKeys.UsernameKey, username.Value);
            await securedStorage.SetAsync(WellKnownStorageKeys.PasswordKey, password.Value);

            databaseConnectionBuilder.ConnectWithCredentials(username.Value, password.Value);
            if (databaseConnectionBuilder.IsEmtpy())
            {
                await databaseConnectionBuilder.MigrateDatabaseAsync(true);
            }

            UserModel? authenticatedUser = await userRepository.FindByUsernameAsync(username.Value);
            if (authenticatedUser != null)
            {
                logger.LogInformation("User {@User} successfully authenticated.");
                return authenticatedUser;
            }

            logger.LogInformation("User {@User} failed to authenticate.");
            return null;
        }

        public async Task<bool> AuthenticateWithExistingCredentialsAsync()
        {
            var username = securedStorage.Get(WellKnownStorageKeys.UsernameKey);
            var password = securedStorage.Get(WellKnownStorageKeys.PasswordKey);
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                logger.LogInformation("Auto authentication failed. No credentials were stored.");
                return false;
            }

            databaseConnectionBuilder.ConnectWithCredentials(username, password);
            if (databaseConnectionBuilder.IsEmtpy())
            {
                logger.LogInformation("Auto authentication failed. The database is empty.");
                return false;
            }

            UserModel? authenticatedUser = await userRepository.FindByUsernameAsync(username);
            if (authenticatedUser != null)
            {
                logger.LogInformation("User {@User} successfully authenticated.");
                return true;
            }

            logger.LogInformation("Auto authentication failed.");
            return false;
        }

        public async Task<UserModel?> GetAuthenticatedUser()
        {
            // username and password have been securely saved during authentication
            var username = securedStorage.Get(WellKnownStorageKeys.UsernameKey);
            var password = securedStorage.Get(WellKnownStorageKeys.PasswordKey);
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return null;
            }

            return await userRepository.FindByUsernameAsync(username);
        }

        public async Task<bool> LogOutAsync()
        {
            var existingUsername = await securedStorage.GetAsync(WellKnownStorageKeys.UsernameKey);
            if (string.IsNullOrEmpty(existingUsername))
            {
                logger.LogInformation("There is no authenticated user found.");
                return false;
            }

            UserModel? user = await GetAuthenticatedUser();
            if (user != null)
            {
                logger.LogDebug("Logging out user {@User}", user.Username);
                return securedStorage.Remove(WellKnownStorageKeys.UsernameKey) && securedStorage.Remove(WellKnownStorageKeys.PasswordKey);
            }

            logger.LogDebug("No authenticated user was found.");
            return false;
        }
    }
}
