using BareTalk.Messenger.Domain.Entities;
using MvvmCross.Plugin.Messenger;

namespace BareTalk.Messenger.Messenger.Messages
{
    public sealed class ConnectOfferAcknowledgedMessage : MvxMessage
    {
        public readonly ContactModel Contact;

        public ConnectOfferAcknowledgedMessage(object sender, ContactModel contact) : base(sender)
        {
            Contact = contact;
        }
    }
}
