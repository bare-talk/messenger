using BareTalk.Messenger.Domain.Entities;
using MvvmCross.Plugin.Messenger;

namespace BareTalk.Messenger.Messenger.Messages
{
    public sealed class MyAcknowledgedConnectOfferMessage : MvxMessage
    {
        public readonly ContactModel Contact;

        public MyAcknowledgedConnectOfferMessage(object sender, ContactModel contact) : base(sender)
        {
            Contact = contact;
        }
    }
}
