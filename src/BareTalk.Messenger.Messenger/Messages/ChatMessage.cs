using BareTalk.Messenger.Domain.Entities;
using MvvmCross.Plugin.Messenger;

namespace BareTalk.Messenger.Messenger.Messages
{
    public sealed class ChatMessage : MvxMessage
    {
        public readonly ConversationMessageModel Message;

        public ChatMessage(object sender, ConversationMessageModel message) : base(sender)
        {
            Message = message;
        }
    }
}
