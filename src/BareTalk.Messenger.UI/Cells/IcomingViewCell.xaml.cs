using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BareTalk.Messenger.UI.Cells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IcomingViewCell : ViewCell
    {
        public IcomingViewCell()
        {
            InitializeComponent();
        }
    }
}
