using BareTalk.Messenger.Core.ViewModels.Contact;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BareTalk.Messenger.UI.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxContentPagePresentation(WrapInNavigationPage = true, NoHistory = false)]
    public partial class Contact : MvxContentPage<ContactViewModel>
    {
        public Contact()
        {
            InitializeComponent();
        }

        private void ContactsSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            var searchbar = sender as SearchBar;
            if (searchbar is null)
            {
                return;
            }

            searchbar.TextColor = !ViewModel.Query.Validate() ? Color.Red : Color.Black;
        }
    }
}
