using BareTalk.Messenger.Core.ViewModels.Login;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BareTalk.Messenger.UI.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxContentPagePresentation(WrapInNavigationPage = true, NoHistory = true)]
    public partial class Login : MvxContentPage<LoginViewModel>
    {
        public Login()
        {
            InitializeComponent();
        }

        private void UsernameEntry_Unfocused(object sender, FocusEventArgs e)
        {
            ViewModel.Username.Validate();
            ViewModel.FormValidationStatus.Validate();
        }

        private void PasswordEntry_Unfocused(object sender, FocusEventArgs e)
        {
            ViewModel.Password.Validate();
            ViewModel.FormValidationStatus.Validate();
        }
    }
}
