using BareTalk.Messenger.Core.ViewModels.Conversation;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Color = Xamarin.Forms.Color;

namespace BareTalk.Messenger.UI.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxContentPagePresentation(WrapInNavigationPage = true)]
    public partial class Conversation : MvxContentPage<ConversationViewModel>
    {
        public Conversation()
        {
            InitializeComponent();
        }

        private void Button_Clicked(object sender, System.EventArgs e)
        {
            ChatBox.Text = string.Empty;
        }

        private void ChatBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!(sender is Entry messageEntry))
            {
                return;
            }

            messageEntry.TextColor = !ViewModel.Message.Validate() ? Color.Red : Color.Black;
        }
    }
}
