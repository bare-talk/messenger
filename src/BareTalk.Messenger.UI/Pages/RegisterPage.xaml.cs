using BareTalk.Messenger.Core.ViewModels.Register;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms.Xaml;

namespace BareTalk.Messenger.UI.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxModalPresentation(WrapInNavigationPage = true)]
    public partial class RegisterPage : MvxContentPage<RegisterViewModel>
    {
        public RegisterPage()
        {
            InitializeComponent();
        }

        private void UsernameEntry_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            ViewModel.Username.Validate();
            ViewModel.FormValidationStatus.Validate();
        }

        private void DisplayNameEntry_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            ViewModel.DisplayName.Validate();
            ViewModel.FormValidationStatus.Validate();
        }

        private void PasswordEntry_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            ViewModel.Password.Validate();
            ViewModel.FormValidationStatus.Validate();
        }

        private void ConfirmPasswordEntry_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            ViewModel.ConfirmPassword.Validate();
            ViewModel.FormValidationStatus.Validate();
        }
    }
}
