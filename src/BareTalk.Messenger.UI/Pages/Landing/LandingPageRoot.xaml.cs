using BareTalk.Messenger.Core.ViewModels.Landing;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms.Xaml;

namespace BareTalk.Messenger.UI.Pages.Landing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxTabbedPagePresentation(TabbedPosition.Root, NoHistory = true)]
    public partial class LandingPageRoot : MvxTabbedPage<LandingPageViewModel>
    {
        public LandingPageRoot()
        {
            InitializeComponent();
        }

        private bool firstTime = true;

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (firstTime)
            {
                ViewModel.ShowInitialViewModelsCommand.ExecuteAsync(null);
                firstTime = false;
            }
        }
    }
}
