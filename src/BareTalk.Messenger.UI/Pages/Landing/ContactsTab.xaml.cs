using BareTalk.Messenger.Core.ViewModels.Landing;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms.Xaml;

namespace BareTalk.Messenger.UI.Pages.Landing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxTabbedPagePresentation(WrapInNavigationPage = false, Title = "Contacts", Position = TabbedPosition.Tab, NoHistory = true)]
    public partial class ContactsTab : MvxContentPage<ContactsTabViewModel>
    {
        public ContactsTab()
        {
            InitializeComponent();
        }
    }
}
