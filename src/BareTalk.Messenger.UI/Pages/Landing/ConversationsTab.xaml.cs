using BareTalk.Messenger.Core.ViewModels.Landing;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms.Xaml;

namespace BareTalk.Messenger.UI.Pages.Landing
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxTabbedPagePresentation(WrapInNavigationPage = false, Title = "Conversations", Position = TabbedPosition.Tab, NoHistory = true)]
    public partial class ConversationsTab : MvxContentPage<ConversationsTabViewModel>
    {
        public ConversationsTab()
        {
            InitializeComponent();
        }
    }
}
