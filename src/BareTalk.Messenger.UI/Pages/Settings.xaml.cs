using BareTalk.Messenger.Core.ViewModels.Settings;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms.Xaml;

namespace BareTalk.Messenger.UI.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    [MvxContentPagePresentation(WrapInNavigationPage = true)]
    public partial class Settings : MvxContentPage<SettingsViewModel>
    {
        public Settings()
        {
            InitializeComponent();
        }
    }
}
