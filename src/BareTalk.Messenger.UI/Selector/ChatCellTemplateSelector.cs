using System;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.UI.Cells;
using Xamarin.Forms;

namespace BareTalk.Messenger.UI.Selector
{
    public class ChatCellTemplateSelector : DataTemplateSelector
    {
        private readonly DataTemplate incomingDataTemplate;
        private readonly DataTemplate outgoingDataTemplate;

        public ChatCellTemplateSelector()
        {
            incomingDataTemplate = new DataTemplate(typeof(IcomingViewCell));
            outgoingDataTemplate = new DataTemplate(typeof(OutgoingViewCell));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if (container is null)
            {
                throw new ArgumentNullException(nameof(container));
            }

            if (!(item is ConversationMessageModel messageVm))
            {
                return null;
            }

            return messageVm.From.Value == messageVm.Me.Value ?
                outgoingDataTemplate : incomingDataTemplate;
        }
    }
}
