using System;
using BareTalk.Messenger.Domain.Contracts.Encoder;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Encoder.MessageDecoder
{
    public sealed class Base64Decoder : IDecode
    {
        private readonly ILogger<Base64Decoder> logger;

        public Base64Decoder(ILogger<Base64Decoder> logger)
        {
            this.logger = logger;
        }

        public string Decode(byte[] data)
        {
            try
            {
                return Convert.ToBase64String(data);
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to convert byte array to base 64 string. Error: {@Error}", exception.Message);
            }

            return string.Empty;
        }
    }
}
