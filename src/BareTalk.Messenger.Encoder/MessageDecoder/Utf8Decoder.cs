using System;
using System.Text;
using BareTalk.Messenger.Domain.Contracts.Encoder;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Encoder.MessageDecoder
{
    public class Utf8Decoder : IDecode
    {
        private readonly ILogger<Utf8Decoder> logger;

        public Utf8Decoder(ILogger<Utf8Decoder> logger)
        {
            this.logger = logger;
        }

        public string Decode(byte[] data)
        {
            try
            {
                return Encoding.UTF8.GetString(data);
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to convert the byte array to UTF8 string. Error: {@Error}", exception.Message);
            }

            return string.Empty;
        }
    }
}
