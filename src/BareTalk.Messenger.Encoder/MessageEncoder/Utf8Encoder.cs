using System;
using System.Text;
using BareTalk.Messenger.Domain.Contracts.Encoder;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Encoder.MessageEncoder
{
    public sealed class Utf8Encoder : IEncode
    {
        private readonly ILogger<Utf8Encoder> logger;

        public Utf8Encoder(ILogger<Utf8Encoder> logger)
        {
            this.logger = logger;
        }

        public byte[] Encode(string data)
        {
            try
            {
                return Encoding.UTF8.GetBytes(data);
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to convert UTF string to byte array. Error: {@Error}", exception.Message);
            }

            return new byte[0];
        }
    }
}
