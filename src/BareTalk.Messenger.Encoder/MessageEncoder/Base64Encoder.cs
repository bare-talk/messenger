using System;
using BareTalk.Messenger.Domain.Contracts.Encoder;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.Encoder.MessageEncoder
{
    public sealed class Base64Encoder : IEncode
    {
        private readonly ILogger<Base64Encoder> logger;

        public Base64Encoder(ILogger<Base64Encoder> logger)
        {
            this.logger = logger;
        }

        public byte[] Encode(string data)
        {
            try
            {
                return Convert.FromBase64String(data);
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to convert the string to a base 64 string. Error: {@Error}", exception.Message);
            }

            return new byte[0];
        }
    }
}
