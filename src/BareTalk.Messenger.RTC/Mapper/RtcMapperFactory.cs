using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Mapping.Factory;
using BareTalk.Messenger.Mapping.Mapper;
using BareTalk.Messenger.RTC.Models;
using Mapster;

namespace BareTalk.Messenger.RTC.Mapper
{
    public sealed class RtcMapperFactory : IMapperFactory
    {
        public IObjectMapper Create()
        {
            TypeAdapterConfig mapperConfiguration = CreateMappings();
            return new AdaptedMapper(mapperConfiguration);
        }

        private TypeAdapterConfig CreateMappings()
        {
            var configuration = new TypeAdapterConfig();
            configuration
                .ForType<AcknowledgedConnectionMessage, MyAcknowledgedConnectOfferModel>()
                .ConstructUsing(src => new MyAcknowledgedConnectOfferModel(
                    src.From,
                    src.ClaimedPublicKey
                ));

            configuration
                .ForType<ChatMessage, EncryptedMessageModel>()
                .ConstructUsing(src => new EncryptedMessageModel(
                    src.EncryptedMessage,
                    src.MessageSignature,
                    src.From
                ));

            configuration
                .ForType<ConnectMessage, IncomingConnectOfferModel>()
                .ConstructUsing(src => new IncomingConnectOfferModel(
                    src.From,
                    src.ClaimedPublicKey,
                    src.DisplayName
                ));

            configuration
               .ForType<AcknowledgedConnectionMessage, MyAcknowledgedConnectOfferModel>()
               .ConstructUsing(src => new MyAcknowledgedConnectOfferModel(
                   src.From,
                   src.ClaimedPublicKey
               ));

            return configuration;
        }
    }
}
