using System;
using System.Collections.Generic;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;
using BareTalk.Messenger.Domain.Contracts.Rtc;
using BareTalk.Messenger.Domain.Entities;
using BareTalk.Messenger.Domain.ValueObjects;
using BareTalk.Messenger.Mapping.Mapper;
using BareTalk.Messenger.RTC.Configuration;
using BareTalk.Messenger.RTC.Models;
using BareTalk.Messenger.Serialization.Deserializer;
using BareTalk.Messenger.Serialization.Serializer;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;

namespace BareTalk.Messenger.RTC.Services
{
    public sealed class RtcClient : IRtcClient
    {
        private readonly ILogger<RtcClient> logger;
        private readonly SignalingConfiguration configuration;
        private readonly ISerializer serializer;
        private readonly IDeserializer deserializer;
        private readonly IObjectMapper mapper;

        private HubConnection hubConnection = null!;
        private CancellationTokenSource cancellationTokenSource;

        private IDisposable? sendMessageDisposable;

        #region Messages

        private readonly Subject<EncryptedMessageModel> newMessageReceived = new Subject<EncryptedMessageModel>();
        public IObservable<EncryptedMessageModel> NewMessageReceived => newMessageReceived;

        #endregion Messages

        #region Contacts

        private readonly Subject<IncomingConnectOfferModel> connectOfferReceived = new Subject<IncomingConnectOfferModel>();
        public IObservable<IncomingConnectOfferModel> ContactOfferReceived => connectOfferReceived;

        #endregion Contacts

        #region Acknowledgment

        private readonly Subject<MyAcknowledgedConnectOfferModel> contactOfferAcknowledged = new Subject<MyAcknowledgedConnectOfferModel>();
        public IObservable<MyAcknowledgedConnectOfferModel> ContactOfferAcknowledged => contactOfferAcknowledged;

        #endregion Acknowledgment

        public RtcClient(
            ILogger<RtcClient> logger,
            SignalingConfiguration configuration,
            ISerializer serializer,
            IDeserializer deserializer,
            IObjectMapper mapper
        )
        {
            this.logger = logger;
            this.configuration = configuration;
            cancellationTokenSource = new CancellationTokenSource();
            this.serializer = serializer;
            this.deserializer = deserializer;
            this.mapper = mapper;
        }

        public async Task StartAsync(Username username)
        {
            if (hubConnection?.State == HubConnectionState.Connected)
            {
                logger.LogWarning("The RTC client is already connected.");
                return;
            }

            if (cancellationTokenSource.IsCancellationRequested)
            {
                logger.LogWarning("Re-creating the cancellation source.");
                cancellationTokenSource = new CancellationTokenSource();
            }

            hubConnection = new HubConnectionBuilder()
                .WithUrl(configuration.HubAddress, options =>
                {
                    options
                        .Headers
                        .Add(new KeyValuePair<string, string>("X-Username", username.Value));
                })
                .Build();

            SetupEvents();
            SetupSbscriptions();

            CancellationToken token = cancellationTokenSource.Token;
            while (!cancellationTokenSource.IsCancellationRequested)
            {
                logger.LogDebug("Attempting to connect to {@Hub}", configuration.HubAddress);

                try
                {
                    await hubConnection.StartAsync(token);
                    return;
                }
                catch (Exception exception)
                {
                    logger.LogError("Encountered error when connection to hub {@Hub}. Retrying to connect in {@Seconds}. Error: {@Error}", configuration.HubAddress, exception.Message, configuration.InitialConnectionRetryTimeoutSeconds);
                }

                await Task.Delay(TimeSpan.FromSeconds(configuration.InitialConnectionRetryTimeoutSeconds), token);
            }

            logger.LogTrace("The hub {@Hub} has been started.", configuration.HubAddress);
        }

        private void SetupEvents()
        {
            hubConnection.Closed += (_) =>
            {
                logger.LogDebug("The hub connection has been closed.");
                return Task.CompletedTask;
            };

            hubConnection.Reconnected += (_) =>
            {
                logger.LogDebug("IN Hub Connection re-established");
                return Task.CompletedTask;
            };

            hubConnection.Reconnecting += _ =>
            {
                logger.LogDebug("Reconnecting to hub...");
                return Task.CompletedTask;
            };
        }

        private void SetupSbscriptions()
        {
            logger.LogTrace("Setting up subscriptions");

            sendMessageDisposable = hubConnection.On("ReceiveMessage", (string jsonEncodedMessage, string from, int messageTypeMarker) =>
            {
                switch (messageTypeMarker)
                {
                    case MessageType.OfferToConnect:
                        {
                            logger.LogInformation("Got new connection offer from {@From}", from);

                            ConnectMessage? offer = deserializer.Deserialize<ConnectMessage>(jsonEncodedMessage);
                            if (offer == null)
                            {
                                logger.LogError("Failed to deserialize the connection offer.");
                                return;
                            }

                            IncomingConnectOfferModel mapped = mapper.Map<ConnectMessage, IncomingConnectOfferModel>(offer);
                            connectOfferReceived.OnNext(mapped);
                            logger.LogTrace("Successfully notified observers about the new contact offer.");
                            break;
                        }
                    case MessageType.ChatMessage:
                        {
                            logger.LogInformation("Got new chat message from {@From}", from);
                            ChatMessage? message = deserializer.Deserialize<ChatMessage>(jsonEncodedMessage);
                            if (message == null)
                            {
                                logger.LogError("Failed to deserialize the encrypted chat message.");
                                return;
                            }

                            EncryptedMessageModel mapped = mapper.Map<ChatMessage, EncryptedMessageModel>(message);
                            newMessageReceived.OnNext(mapped);
                            logger.LogTrace("Successfully notified observers about the new chat message.");
                            break;
                        }
                    case MessageType.AcknowledgeConnectOffer:
                        {
                            logger.LogInformation("Got new connection acknowledgment from {@From}", from);
                            AcknowledgedConnectionMessage? message = deserializer.Deserialize<AcknowledgedConnectionMessage>(jsonEncodedMessage);
                            if (message == null)
                            {
                                logger.LogError("Failed to deserialize the connect acknowledgment message.");
                                return;
                            }

                            MyAcknowledgedConnectOfferModel mapped = mapper.Map<AcknowledgedConnectionMessage, MyAcknowledgedConnectOfferModel>(message);
                            contactOfferAcknowledged.OnNext(mapped);
                            logger.LogTrace("Successfully notified observers about the new connect acknowledgment message.");
                            break;
                        }
                    default:
                        {
                            logger.LogError("MEssage type {@Type} is not supported.", messageTypeMarker);
                            break;
                        }
                }

                logger.LogInformation("New message received from {@User}", from);
            });

            logger.LogTrace("Finished setting up subscriptions");
        }

        public async Task StopAsync()
        {
            if (hubConnection == null)
            {
                logger.LogWarning("The RTC client is already disconnected.");
                return;
            }

            cancellationTokenSource?.Cancel();
            logger.LogDebug("Attempting to disconnect from {@Hub}", configuration.HubAddress);

            try
            {
                await hubConnection.StopAsync();
                await hubConnection.DisposeAsync();
            }
            catch (Exception exception)
            {
                logger.LogError("Encountered error when  disconnecting from hub {@Hub}.  Error: {@Error}", configuration.HubAddress, exception.Message);
            }

            connectOfferReceived.OnCompleted();
            newMessageReceived.OnCompleted();
            contactOfferAcknowledged.OnCompleted();

            logger.LogTrace("The hub {@Hub} has been stopped.", configuration.HubAddress);
        }

        public async ValueTask DisposeAsync()
        {
            if (hubConnection != null)
            {
                await hubConnection.DisposeAsync();
                logger.LogTrace("The hub {@Hub} has been disposed.", configuration.HubAddress);
            }

            connectOfferReceived.Dispose();
            newMessageReceived.Dispose();
            contactOfferAcknowledged.Dispose();
            sendMessageDisposable?.Dispose();
        }

        public async Task<bool> OfferToConnectAsync(Username to, PublicKey claimedPublicKey, Username from, DisplayName myDisplayName)
        {
            var connectMessage = new ConnectMessage
            {
                ClaimedPublicKey = claimedPublicKey,
                From = from,
                To = to,
                DisplayName = myDisplayName
            };
            var serializedConnectMessage = serializer.Serialize(connectMessage);
            if (serializedConnectMessage == null)
            {
                logger.LogError("Aborting connection offer, failed to serialize offer.");
                return false;
            }

            try
            {
                logger.LogTrace("Attempting to connect to {@User}", to.Value);
                await hubConnection.InvokeAsync("SendMessage", serializedConnectMessage, from.Value, to.Value, connectMessage.MessageTypeMarker);
                logger.LogInformation("Successfully connected.");
                return true;
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to connect to {@Username}. Error {@Error}", to.Value, exception.Message);
            }

            return false;
        }

        public async Task<bool> SendMessageAsync(Message encryptedMessage, Username from, Username to, Signature messageSignature)
        {
            var chatMessage = new ChatMessage
            {
                EncryptedMessage = encryptedMessage,
                From = from,
                MessageSignature = messageSignature,
                To = to
            };
            var serializedChatMessage = serializer.Serialize(chatMessage);
            if (serializedChatMessage == null)
            {
                logger.LogError("Aborting sending chat message, failed to serialize object.");
                return false;
            }

            try
            {
                logger.LogTrace("Attempting to dispatch a message to {@User}", to.Value);
                await hubConnection.InvokeAsync("SendMessage", serializedChatMessage, from.Value, to.Value, chatMessage.MessageTypeMarker);
                logger.LogInformation("Successfully connected.");
                return true;
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to dispatch a message to {@User}. Error {@Error}", to.Value, exception.Message);
            }

            return false;
        }

        public async Task<bool> AcknowledgeConnectionAsync(Username from, Username to, PublicKey claimedPublicKey)
        {
            var ack = new AcknowledgedConnectionMessage
            {
                ClaimedPublicKey = claimedPublicKey,
                From = from
            };
            var serializedChatMessage = serializer.Serialize(ack);
            if (serializedChatMessage == null)
            {
                logger.LogError("Aborting sending connect acknowledgment, failed to serialize object.");
                return false;
            }

            try
            {
                logger.LogTrace("Attempting to ACK connection to {@User}", to.Value);
                await hubConnection.InvokeAsync("SendMessage", serializedChatMessage, from.Value, to.Value, ack.MessageTypeMarker);
                logger.LogInformation("Successfully ACKed connection.");
                return true;
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to ACK connection to {@User}. Error {@Error}", to.Value, exception.Message);
            }

            return false;
        }
    }
}
