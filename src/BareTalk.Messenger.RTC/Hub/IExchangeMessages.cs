using System.Threading.Tasks;

namespace BareTalk.Messenger.RTC.Hub
{
    public interface IExchangeMessages
    {
        Task ConnectAsync(string to, string claimedPublicKey, string from);

        Task SendMessageAsync(string encryptedMessage, string from, string to, string messageSignature);
    }
}
