using BareTalk.Messenger.Serialization.Deserializer;
using BareTalk.Messenger.Serialization.Serializer;

namespace BareTalk.Messenger.RTC.Models
{
    public interface IRtcModel : IAmSerializable, IAmDeSerializable
    {
        public int MessageTypeMarker { get; }
    }
}
