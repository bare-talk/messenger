using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.RTC.Models
{
    public class AcknowledgedConnectionMessage : IRtcModel
    {
        public PublicKey? ClaimedPublicKey { get; set; }
        public Username? From { get; set; }

        public int MessageTypeMarker => MessageType.AcknowledgeConnectOffer;
    }
}
