using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.RTC.Models
{
    public class ChatMessage : IRtcModel
    {
        public Message? EncryptedMessage { get; set; }
        public Signature? MessageSignature { get; set; }
        public Username? From { get; set; }
        public Username? To { get; set; }

        public int MessageTypeMarker => MessageType.ChatMessage;
    }
}
