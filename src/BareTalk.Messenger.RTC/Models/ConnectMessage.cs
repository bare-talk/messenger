using BareTalk.Messenger.Domain.ValueObjects;

namespace BareTalk.Messenger.RTC.Models
{
    public class ConnectMessage : IRtcModel
    {
        public Username? From { get; set; }
        public Username? To { get; set; }
        public DisplayName? DisplayName { get; set; }
        public PublicKey? ClaimedPublicKey { get; set; }
        public int MessageTypeMarker => MessageType.OfferToConnect;
    }
}
