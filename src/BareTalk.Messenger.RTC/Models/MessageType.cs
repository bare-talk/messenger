namespace BareTalk.Messenger.RTC.Models
{
    public static class MessageType
    {
        public const int ChatMessage = 1;
        public const int OfferToConnect = 2;
        public const int AcknowledgeConnectOffer = 3;
    }
}
