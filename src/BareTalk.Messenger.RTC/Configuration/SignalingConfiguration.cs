namespace BareTalk.Messenger.RTC.Configuration
{
    public sealed class SignalingConfiguration
    {
        public string HubAddress { get; set; } = "";
        public int InitialConnectionRetryTimeoutSeconds { get; set; } = 5;
    }
}
