using System;
using System.Net.Http;
using BareTalk.Messenger.Application.Facades.Authentication;
using BareTalk.Messenger.Application.Facades.Contact;
using BareTalk.Messenger.Application.Facades.Conversation;
using BareTalk.Messenger.Application.Facades.Messaging;
using BareTalk.Messenger.Application.Facades.Registration;
using BareTalk.Messenger.Application.Services;
using BareTalk.Messenger.Authentication.Services;
using BareTalk.Messenger.DataAccess.Configuration.Factory;
using BareTalk.Messenger.DataAccess.Connection.Builder;
using BareTalk.Messenger.DataAccess.Mapper;
using BareTalk.Messenger.DataAccess.Repository;
using BareTalk.Messenger.DevicePreferences.DevicePreferences;
using BareTalk.Messenger.Domain.Contracts.Application;
using BareTalk.Messenger.Domain.Contracts.Authentication;
using BareTalk.Messenger.Domain.Contracts.DataAccess;
using BareTalk.Messenger.Domain.Contracts.Encoder;
using BareTalk.Messenger.Domain.Contracts.Encryption;
using BareTalk.Messenger.Domain.Contracts.Pki;
using BareTalk.Messenger.Domain.Contracts.Rtc;
using BareTalk.Messenger.Domain.Contracts.SecuredStorage;
using BareTalk.Messenger.Domain.Repository;
using BareTalk.Messenger.Encoder.MessageDecoder;
using BareTalk.Messenger.Encoder.MessageEncoder;
using BareTalk.Messenger.Encryption.Decryptor;
using BareTalk.Messenger.Encryption.Derivation;
using BareTalk.Messenger.Encryption.Encryptor;
using BareTalk.Messenger.Encryption.Exchange;
using BareTalk.Messenger.Encryption.Generator;
using BareTalk.Messenger.Encryption.Salt;
using BareTalk.Messenger.Encryption.Sign;
using BareTalk.Messenger.Mapping.Mapper;
using BareTalk.Messenger.Pki.Claimer;
using BareTalk.Messenger.Pki.Http;
using BareTalk.Messenger.Pki.Mapper;
using BareTalk.Messenger.Pki.Publisher;
using BareTalk.Messenger.Pki.Rest;
using BareTalk.Messenger.RTC.Configuration;
using BareTalk.Messenger.RTC.Mapper;
using BareTalk.Messenger.RTC.Services;
using BareTalk.Messenger.SecuredStorage.Storage;
using BareTalk.Messenger.Serialization.Deserializer;
using BareTalk.Messenger.Serialization.Serializer;
using Microsoft.Extensions.Logging;
using MvvmCross.IoC;

namespace BareTalk.Messenger.Bootstrapper
{
    public static class DependencyInjectionExtensions
    {
        public static IMvxIoCProvider BootstrapApplicationDependencies(this IMvxIoCProvider provider, ILogger logger)
        {
            // order matters due to custom factories
            try
            {
                return provider
                        .RegisterEncoder()
                        .RegisterSecuredStorage()
                        .RegisterDevicePreferences()
                        .RegisterDataAccess()
                        .RegisterApplication()
                        .RegisterServices()
                        .RegisterEncryption()
                        .RegisterRtc()
                        .RegisterAuthentication()
                        .RegisterPki();
            }
            catch (Exception exception)
            {
                logger.LogCritical("Dependency injection issue: {@Error}", exception.Message);
            }

            return provider;
        }

        private static IMvxIoCProvider RegisterDataAccess(this IMvxIoCProvider provider)
        {
            // database
            provider.RegisterType<IDatabaseConfigurationFactory, DatabaseConfigurationFactory>();
            provider.RegisterSingleton<IDatabaseConnectionBuilder>(() => new DatabaseConnectionBuilder(
                    provider.Resolve<IDatabaseConfigurationFactory>(),
                    provider.Resolve<ISecuredStorage>(),
                    provider.Resolve<ILogger<IDatabaseConnection>>()
            ));

            provider.RegisterSingleton(() => provider.Resolve<IDatabaseConnectionBuilder>().InitializeConnection());

            // mapper
            provider.RegisterType<DataAccessMapperFactory>();
            IObjectMapper mapper = provider.Resolve<DataAccessMapperFactory>().Create();

            // repositories
            provider.RegisterType<IUserRepository>(() => new UserRepository(
                    provider.Resolve<IDatabaseConnection>(),
                    mapper,
                    provider.Resolve<ILogger<UserRepository>>()
            ));
            provider.RegisterType<IKeyPairRepository>(() => new KeyPairRepository(
                    provider.Resolve<IDatabaseConnection>(),
                    mapper
            ));
            provider.RegisterType<IContactRepository>(() => new ContactRepository(
                   provider.Resolve<IDatabaseConnection>(),
                   mapper
            ));
            provider.RegisterType<IMessageRepository>(() => new MessageRepository(
                    provider.Resolve<IDatabaseConnection>(),
                    mapper
            ));
            provider.RegisterType<IConversationRepository>(() => new ConversationRepository(
              provider.Resolve<IDatabaseConnection>(),
              mapper
            ));

            return provider;
        }

        private static IMvxIoCProvider RegisterSecuredStorage(this IMvxIoCProvider provider)
        {
            provider.RegisterType<ISecuredStorage, DeviceSecuredStorage>();

            return provider;
        }

        private static IMvxIoCProvider RegisterServices(this IMvxIoCProvider provider)
        {
            return provider;
        }

        private static IMvxIoCProvider RegisterRtc(this IMvxIoCProvider provider)
        {
            provider.RegisterType<RtcMapperFactory>();
            IObjectMapper mapper = provider.Resolve<RtcMapperFactory>().Create();
            provider.RegisterSingleton(new SignalingConfiguration
            {
                HubAddress = "http://10.0.2.2:5186/exchange"
            });
            provider.RegisterSingleton<IRtcClient>(() => new RtcClient(
                    provider.Resolve<ILogger<RtcClient>>(),
                    provider.Resolve<SignalingConfiguration>(),
                    provider.Resolve<ISerializer>(),
                    provider.Resolve<IDeserializer>(),
                    mapper
            ));

            return provider;
        }

        private static IMvxIoCProvider RegisterApplication(this IMvxIoCProvider provider)
        {
            // services
            provider.RegisterType<IKeyPairService, KeyPairService>();
            provider.RegisterType<IContactService, ContactService>();
            provider.RegisterType<IMessagesService, MessagesService>();
            provider.RegisterType<IConversationService, ConversationsService>();

            // facades
            provider.RegisterType<IUserRegistrationFacade, UserRegistrationFacade>();
            provider.RegisterType<IAuthenticationFacade, AuthenticationFacade>();
            provider.RegisterType<IContactFacade, ContactFacade>();
            provider.RegisterType<IMessageFacade, MessageFacade>();
            provider.RegisterType<IConversationFacade, ConversationFacade>();

            return provider;
        }

        private static IMvxIoCProvider RegisterAuthentication(this IMvxIoCProvider provider)
        {
            // authentication service
            provider.RegisterType<IAuthenticationService, AuthenticationService>();

            // registration service
            provider.RegisterType<IRegistrationService, RegistrationService>();

            return provider;
        }

        private static IMvxIoCProvider RegisterDevicePreferences(this IMvxIoCProvider provider)
        {
            provider.RegisterType<IDevicePreferences, ManagedPreferences>();

            return provider;
        }

        private static IMvxIoCProvider RegisterEncoder(this IMvxIoCProvider provider)
        {
            provider.RegisterType<IDecode, Base64Decoder>();
            provider.RegisterType<IEncode, Base64Encoder>();
            provider.RegisterType<Utf8Decoder>();
            return provider;
        }

        private static IMvxIoCProvider RegisterEncryption(this IMvxIoCProvider provider)
        {
            provider.RegisterType<IDecrypt>(() => new AesDecryptor(provider.Resolve<Utf8Decoder>()));
            provider.RegisterType<IDeriveBytes, Pbkdf2Wrapper>();
            provider.RegisterType<IExchangeKeys, Ed25519KeyExchange>();
            provider.RegisterType<IEncrypt, AesEncryptor>();
            provider.RegisterType<IGenerateSalt, SecureSaltGenerator>();
            provider.RegisterType<IExchangeKeys, Ed25519KeyExchange>();
            provider.RegisterType<IGenerateKeys, Ed25519KeyPairGenerator>();
            provider.RegisterType<IGenerateSalt, SecureSaltGenerator>();
            provider.RegisterType<ISignMessages, Ed25519Signer>();

            return provider;
        }

        private static IMvxIoCProvider RegisterPki(this IMvxIoCProvider provider)
        {
            provider.RegisterType<ISerializer, Serializer>();
            provider.RegisterType<IDeserializer, JsonDeserializer>();
            provider.RegisterType(() => new HttpClient()
            {
                BaseAddress = new Uri("http://10.0.2.2:5159"),
                Timeout = TimeSpan.FromSeconds(30),
            });
            provider.RegisterType<IHttpClient, CustomHttpClient>();

            // mapper
            provider.RegisterType<PkiMapperFactory>();
            IObjectMapper pkiMapper = provider.Resolve<PkiMapperFactory>().Create();

            // services
            provider.RegisterType<IPkiRestService>(() => new PkiRestService(
                provider.Resolve<ISerializer>(),
                provider.Resolve<IDeserializer>(),
                provider.Resolve<ILogger<PkiRestService>>(),
                provider.Resolve<IHttpClient>()
            ));
            provider.RegisterType<IClaimPublicKeys>(() => new PublicKeyClaimer(
                provider.Resolve<IPkiRestService>(),
                pkiMapper,
                provider.Resolve<IKeyPairService>(),
                provider.Resolve<ILogger<PublicKeyClaimer>>()
            ));
            provider.RegisterType<IPublishKeys>(() => new KeyPublisher(
                provider.Resolve<ILogger<KeyPublisher>>(),
                provider.Resolve<IPkiRestService>(),
                pkiMapper
            ));

            return provider;
        }
    }
}
