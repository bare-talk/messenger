using Xamarin.UITest;

namespace BareTalk.Messenger.UI.Test
{
    public static class AppInitializer
    {
        public static IApp StartApp(Platform platform)
        {
            return platform == Platform.Android
                ? ConfigureApp.Android.StartApp()
                : (IApp)ConfigureApp.iOS.StartApp();
        }
    }
}
