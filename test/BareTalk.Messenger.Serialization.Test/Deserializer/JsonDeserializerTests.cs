using BareTalk.Messenger.RTC.Models;
using BareTalk.Messenger.Serialization.Deserializer;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace BareTalk.Messenger.Serialization.Test.Deserializer
{
    public class JsonDeserializerTests
    {
        private readonly JsonDeserializer deserializer;

        public JsonDeserializerTests()
        {
            deserializer = new JsonDeserializer(new Mock<ILogger<JsonDeserializer>>().Object);
        }

        [Fact]
        public void Calling_Deserialize_ShouldDeserialize_JsonObject_Successfully()
        {
            const string json = "{\"From\":{\"Value\":\"catalinmodan18\"},\"To\":{\"Value\":\"catalinmodan19\"},\"DisplayName\":{\"Value\":\"catalin modan 18\"},\"ClaimedPublicKey\":{\"Value\":\"yJOf2NA1yh87w\u002BKAGLVAvgLD0soPkIPsN / cpmXTTGM8 = \"},\"MessageTypeMarker\":2}";

            ConnectMessage? actual = deserializer.Deserialize<ConnectMessage>(json);
            actual.Should().NotBeNull();
        }
    }
}
